# This class contains structure of loaded and modified data
from collections import Counter
import datetime as dt


class DataModel:
    def __init__(self, catalog, file_name, episode_length, time_stamp,
                 quotes):
        self.catalog = catalog
        self.file_name = file_name
        self.episode_length = episode_length
        self.time_stamp = time_stamp
        self.quotes = quotes

    def create_word_list(self):
        word_list = []

        for elem in self.quotes:
            elem = elem.lower()
            tmp = elem.split(" ")
            for x in tmp:
                word_list.append(x)

        return word_list

    def time_ratio(self):

        ratio = self.quotes_time().total_seconds() / \
                self.episode_length.total_seconds()
        return ratio

    def quotes_time(self):
        quotes_time = sum(self.time_stamp, dt.timedelta(0, 0, 0))
        return quotes_time

    def filter_list(self, x):
        word_dictionary = ["the", "and", "that", "have", "for", "with", "you",
                           "this", "but", "his", "from", "they", "say", "her",
                           "she", "will", "all", "would", "there", "their",
                           "what", "was", "just", "its", "are", "dont", "not"]

        if len(x) > 2:
            is_allowed = x not in word_dictionary
        else:
            is_allowed = False

        return is_allowed

    def info(self):
        print("Nazwa odcinka: " + self.file_name)
        print("Dlugosc odcinka: " + str(self.episode_length))
        print("Dlugosc wypowiedzi: " + str(self.quotes_time()))
        print("Ilosc wypowiedzi: " + str(len(self.quotes)))
        print("Stosunek trwania napisow: " + str(round(self.time_ratio(), 2)))

        word_list = self.create_word_list()
        filtred_list = filter(self.filter_list, word_list)
        print(Counter(filtred_list).most_common(10))

        #print('\n'.join(self.quotes))
        print("\n")



