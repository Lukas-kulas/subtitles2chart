1
00:00:00,194 --> 00:00:09,966
Synchro: Anyone Sixe Guilamu
Transcript: Anyone
Suggestions critics insults: itcrowdsub@gmail.com
www.forom.com

2
00:00:30,291 --> 00:00:34,436
The IT Crowd - 1x02 - Calamity Jen

3
00:00:48,601 --> 00:00:51,692
Has this... ever happened to you?

4
00:01:05,376 --> 00:01:10,129
<i>From today, dialling 999
won't get you the emergency services.</i>

5
00:01:10,239 --> 00:01:12,592
<i>And that's not the only thing
that's changing.</i>

6
00:01:13,217 --> 00:01:15,851
<i>Nicer ambulances,
faster response times,</i>

7
00:01:15,865 --> 00:01:17,505
<i>and better looking drivers</i>

8
00:01:17,522 --> 00:01:19,754
<i>mean they're not just
the emergency services,</i>

9
00:01:19,785 --> 00:01:22,494
<i>they're "your" emergency services. So...</i>

10
00:01:22,558 --> 00:01:24,273
<i>Remember the new number:</i>

11
00:01:36,031 --> 00:01:37,636
<i>That's...</i>

12
00:01:48,412 --> 00:01:52,737
<i>Hello? I've had a bit of a tumble...</i>

13
00:01:52,916 --> 00:01:54,865
Well that's easy to remember!

14
00:01:54,961 --> 00:02:03,616
0118 999 881 999 119 725...

15
00:02:03,699 --> 00:02:04,598
...3!

16
00:02:05,369 --> 00:02:07,490
I don't know why
they just couldn't keep it as it was!

17
00:02:07,536 --> 00:02:09,640
How hard is it to remember 911?

18
00:02:09,675 --> 00:02:11,233
- You mean 999...
- I mean 999!

19
00:02:11,260 --> 00:02:13,127
- That's the American one!
- Yeah!

20
00:02:13,198 --> 00:02:15,941
- You berk!
- Come on, are you ready?

21
00:02:16,897 --> 00:02:18,536
I don't know what to say really
it's fun...

22
00:02:18,559 --> 00:02:20,289
there are some many issues in the world,

23
00:02:20,306 --> 00:02:21,711
They are beautiful, I love them...

24
00:02:21,722 --> 00:02:24,264
in fact they're the most beautiful shoes
I've ever seen but...

25
00:02:24,298 --> 00:02:26,102
you know, I can't have everything
that I love,

26
00:02:26,124 --> 00:02:28,469
otherwise I'd own lots of things,
and actually they're red,

27
00:02:28,528 --> 00:02:31,630
which is quite tarty, and they don't
necessarily go with any of my outfits...

28
00:02:31,970 --> 00:02:33,981
Come on you crazy bitch!

29
00:02:34,072 --> 00:02:34,874
Come on!

30
00:02:35,938 --> 00:02:36,906
Come on!

31
00:02:37,068 --> 00:02:39,251
- Denholm's called the general.
- Oh God, another one!

32
00:02:39,269 --> 00:02:39,840
Yeah!

33
00:02:40,270 --> 00:02:42,616
I bet he declares war on something!

34
00:02:43,076 --> 00:02:45,005
He loves declaring war!

35
00:02:46,168 --> 00:02:47,695
I am declaring...

36
00:02:47,781 --> 00:02:48,853
WAR!

37
00:02:52,482 --> 00:02:54,504
I can see that got your attention.

38
00:02:56,862 --> 00:02:58,547
What am I declaring war on?

39
00:02:58,800 --> 00:03:00,269
My bollocks?!

40
00:03:01,414 --> 00:03:02,883
Stress!

41
00:03:04,124 --> 00:03:07,808
Stress is a disease, people,
and I am a cure!

42
00:03:08,210 --> 00:03:09,852
I'm a doctor with a cure.

43
00:03:10,024 --> 00:03:12,444
No, no! I'm a General!
And it's still a war!

44
00:03:12,660 --> 00:03:14,225
A war on disease!

45
00:03:14,226 --> 00:03:15,303
Stress!

46
00:03:16,786 --> 00:03:18,319
There's too much stress around here.

47
00:03:18,527 --> 00:03:20,436
<i>They're very expensive and besides,</i>

48
00:03:20,450 --> 00:03:23,044
<i>the woman in the shop said
they were way too small for me.</i>

49
00:03:23,057 --> 00:03:25,214
<i>- You see you know, be practical, go on!
- In the time...</i>

50
00:03:25,269 --> 00:03:26,496
I've been speaking,

51
00:03:26,497 --> 00:03:29,671
over 18 million people
have died of stress.

52
00:03:34,517 --> 00:03:35,609
That's another one.

53
00:03:36,553 --> 00:03:37,683
That's another one.

54
00:03:38,319 --> 00:03:39,230
More...

55
00:03:40,411 --> 00:03:41,458
Mayhem!

56
00:03:41,829 --> 00:03:43,044
We gotta deal with it!

57
00:03:49,014 --> 00:03:50,709
- Are you ready to deal with it?!
- Yeah!

58
00:03:50,775 --> 00:03:52,379
Say: "I hate stress!"

59
00:03:52,396 --> 00:03:53,529
- I hate stress!
- No!

60
00:03:53,648 --> 00:03:54,987
You didn't let me finish.

61
00:03:55,203 --> 00:03:56,715
Say: "I hate stress..."

62
00:03:56,845 --> 00:03:59,367
and I want to limitate
its influence in my life,

63
00:03:59,561 --> 00:04:01,656
what can I do about it? Anyone?

64
00:04:02,986 --> 00:04:03,981
Have a bath?

65
00:04:05,044 --> 00:04:06,442
Have a bath?!

66
00:04:06,675 --> 00:04:07,850
Get a bike!

67
00:04:08,421 --> 00:04:10,603
I cycle to work... everyday!

68
00:04:10,666 --> 00:04:11,967
70 miles!

69
00:04:12,100 --> 00:04:14,046
Both here, and here...

70
00:04:14,269 --> 00:04:16,366
are as red as a fire engine!

71
00:04:17,349 --> 00:04:18,441
Any other ideas?

72
00:04:20,200 --> 00:04:21,248
Jen, what about you?

73
00:04:21,306 --> 00:04:22,218
Shoes!

74
00:04:22,508 --> 00:04:24,306
- What?
- Shoes!

75
00:04:24,776 --> 00:04:26,379
Well, shoes...

76
00:04:27,487 --> 00:04:28,719
Ok, look!

77
00:04:28,800 --> 00:04:31,466
I have invited Dr. Julian Holmes

78
00:04:31,488 --> 00:04:33,083
to come and give a class at lunchtime.

79
00:04:33,101 --> 00:04:35,117
He is Europe's leading stress expert.

80
00:04:35,284 --> 00:04:37,391
They call him:" Stressperts!"

81
00:04:37,440 --> 00:04:39,189
Anyone interested in coming along
picking up

82
00:04:39,230 --> 00:04:40,689
a few tips on how to manage stress?

83
00:04:43,904 --> 00:04:45,459
There's a free buffet...

84
00:04:49,106 --> 00:04:49,900
Good!

85
00:04:50,320 --> 00:04:51,680
Good people.

86
00:04:51,933 --> 00:04:53,367
Oh! By the way!

87
00:04:53,494 --> 00:04:55,899
Anyone still experiencing stress
at the end of the day...

88
00:04:56,707 --> 00:04:59,527
will be fired!

89
00:05:00,197 --> 00:05:01,362
It's not going on!

90
00:05:01,415 --> 00:05:03,738
Yeah, I think it is so.
Just down angle it a bit more.

91
00:05:03,767 --> 00:05:05,511
Are you sure you're a 5?

92
00:05:05,563 --> 00:05:07,610
- I'm not a liar.
- Yeah, but I mean I can't...

93
00:05:09,174 --> 00:05:09,839
Just... just...

94
00:05:09,924 --> 00:05:11,948
I  can feel the heel going into the shoe.

95
00:05:11,961 --> 00:05:13,045
Just down angle a bit more...

96
00:05:13,062 --> 00:05:14,489
There is no angle, but I could...

97
00:05:14,510 --> 00:05:15,722
Don't let me push you away!

98
00:05:15,736 --> 00:05:17,438
Just lean in with the shoe. Lean in...

99
00:05:17,573 --> 00:05:18,727
with the shoe!

100
00:05:18,820 --> 00:05:19,524
Lean in!

101
00:05:31,413 --> 00:05:33,405
We've been here... for half an hour.

102
00:05:33,880 --> 00:05:35,276
The shoes aren't going on!

103
00:05:35,290 --> 00:05:37,352
They're two sizes too small.

104
00:05:37,485 --> 00:05:38,980
And even if they did go on,

105
00:05:39,096 --> 00:05:40,271
they would cripple you.

106
00:05:40,579 --> 00:05:42,324
You don't want that, do you?

107
00:06:00,161 --> 00:06:02,726
I myself, have been subject to some...

108
00:06:03,064 --> 00:06:06,169
terrible rages, but... Yes?

109
00:06:06,608 --> 00:06:09,978
- I'm sorry. Is this the stress class?
- Oh yeah. Come on in.

110
00:06:10,241 --> 00:06:11,222
Thank you.

111
00:06:12,728 --> 00:06:14,161
Moss!

112
00:06:14,533 --> 00:06:18,988
This is in here! Moss!

113
00:06:23,593 --> 00:06:25,435
Just come in.

114
00:06:55,271 --> 00:06:56,418
Thank you.

115
00:07:11,774 --> 00:07:12,316
Ok.

116
00:07:15,450 --> 00:07:16,401
Are  you all right?

117
00:07:18,893 --> 00:07:20,621
As I was saying...

118
00:07:20,811 --> 00:07:23,490
I myself have been subject
to some terrible rages.

119
00:07:23,768 --> 00:07:25,067
Where I've...

120
00:07:25,164 --> 00:07:27,621
snapped at my wife, or spoken harshly

121
00:07:27,869 --> 00:07:30,192
to a work colleague, because of stress.

122
00:07:30,330 --> 00:07:34,305
If I... just... thinking about it now,
I realise I'm...

123
00:07:34,388 --> 00:07:35,425
raising my voice.

124
00:07:35,881 --> 00:07:37,208
I'm sorry.

125
00:07:39,530 --> 00:07:42,158
Stress is the main cause
of heart attacks.

126
00:07:42,307 --> 00:07:45,277
And even a little bit... well...

127
00:07:46,484 --> 00:07:48,706
Let me show you what stress can do.

128
00:07:53,571 --> 00:07:55,466
I'm gonna need a volunteer.

129
00:07:56,503 --> 00:07:57,778
Yes. Come on up.

130
00:08:01,385 --> 00:08:04,364
That's fine. Can I have your left hand
there? The back?

131
00:08:04,434 --> 00:08:06,018
And just relax there.

132
00:08:06,249 --> 00:08:08,986
I'm gonna ask you
a very personal question.

133
00:08:10,410 --> 00:08:11,471
There we are, you see?

134
00:08:11,813 --> 00:08:13,534
Sorry, I'm not gonna ask you
anything at all.

135
00:08:13,542 --> 00:08:15,740
That's just to show how little is needed

136
00:08:15,758 --> 00:08:17,890
to send downstress levels shooting up.

137
00:08:17,898 --> 00:08:19,360
Just a simple statement like that

138
00:08:19,372 --> 00:08:21,346
is registered very strongly
on the display.

139
00:08:21,376 --> 00:08:22,815
- Yes, another volunteer?
- Yeah.

140
00:08:22,992 --> 00:08:23,531
Ok.

141
00:08:23,612 --> 00:08:24,772
I'll have... just this off.

142
00:08:27,151 --> 00:08:29,639
Ah! There it goes! Yes, raise up!

143
00:08:29,687 --> 00:08:31,588
Look at that! Brilliant!

144
00:08:31,711 --> 00:08:35,136
Brilliant, excellent!
It's a super machine!

145
00:08:42,202 --> 00:08:43,827
Wow, great shoes!

146
00:08:43,926 --> 00:08:44,995
Thank you very much.

147
00:08:46,310 --> 00:08:48,477
I really envy women with dainty feet.

148
00:08:48,588 --> 00:08:49,948
What are you? A five?

149
00:08:50,409 --> 00:08:52,763
Yeah! Five...

150
00:08:55,873 --> 00:09:00,025
Well, just a little
misunderstanding there I imagine. So...

151
00:09:00,216 --> 00:09:02,014
- Huh let's move on... with...
- Wow!

152
00:09:02,257 --> 00:09:04,796
I actually haven't had a proper go yet.

153
00:09:04,878 --> 00:09:07,426
Well, I don't need attach... can't,
what's happened there...

154
00:09:07,479 --> 00:09:09,196
Actually, I don't think that's fair...

155
00:09:09,210 --> 00:09:10,394
I would like a go, and I think

156
00:09:10,505 --> 00:09:13,038
Roy should be punished for
nearly killing that lady.

157
00:09:13,093 --> 00:09:15,042
Well, I think I should go first!

158
00:09:15,049 --> 00:09:16,110
I see, well let's move on...

159
00:09:16,124 --> 00:09:17,851
- When did you grow a bit?
- A little go.

160
00:09:17,878 --> 00:09:19,820
- We gotta move on...
- And I think we could...

161
00:09:20,100 --> 00:09:21,096
Can we just move on?

162
00:09:21,136 --> 00:09:23,114
You're old ? Are you very old?

163
00:09:23,133 --> 00:09:24,216
Let's move on!

164
00:09:25,930 --> 00:09:27,296
Oh! Shut up!

165
00:09:31,517 --> 00:09:32,589
What are you eating?

166
00:09:32,831 --> 00:09:33,850
Muffin.

167
00:09:34,040 --> 00:09:35,751
A muffin? How can you be hungry,

168
00:09:35,779 --> 00:09:37,834
you ate a whole chicken
at the stress buffet!

169
00:09:39,225 --> 00:09:40,027
What's that?

170
00:09:40,331 --> 00:09:42,015
Oh, I've made a stress machine

171
00:09:42,043 --> 00:09:44,641
to try out for myself,
like the one in that class.

172
00:09:44,825 --> 00:09:45,738
Watch this.

173
00:09:46,605 --> 00:09:49,686
I'm  going to ask myself
a very personal question.

174
00:09:52,737 --> 00:09:56,262
Don't worry, I'm not really going
to ask myself any questions.

175
00:09:57,802 --> 00:09:59,026
Pretty good.

176
00:09:59,111 --> 00:10:01,947
Not as good as the one
in the stress class.

177
00:10:22,913 --> 00:10:24,469
The shoes!

178
00:10:26,612 --> 00:10:28,029
What was all that about?

179
00:10:28,253 --> 00:10:30,897
Well, like all the women,
she's shoe mad!

180
00:10:30,936 --> 00:10:32,457
It's a bit sexist, isn't it?

181
00:10:32,629 --> 00:10:35,032
Do you know one woman
who isn't obsessed with shoes?

182
00:10:35,118 --> 00:10:37,053
No, but I only know one woman.

183
00:10:37,226 --> 00:10:40,381
And she just left the room shouting:
"the shoes!".

184
00:11:01,757 --> 00:11:04,439
The needle's freaking out!

185
00:11:08,335 --> 00:11:10,260
Try not to hurt yourself!

186
00:11:14,268 --> 00:11:15,975
- Moss?
- Huh?

187
00:11:16,201 --> 00:11:19,182
Did you use a soldering iron
to make that stress machine?

188
00:11:19,226 --> 00:11:19,962
Yes.

189
00:11:20,332 --> 00:11:21,536
You turned it off?

190
00:11:21,626 --> 00:11:23,804
Oh I'm fairly sure I did!

191
00:11:25,798 --> 00:11:29,128
Because, you remember
what happened last time, right?

192
00:11:29,196 --> 00:11:31,032
Yes! That was very funny!

193
00:11:31,049 --> 00:11:32,363
Well no! No.

194
00:11:33,443 --> 00:11:36,141
It was very dangerous
and someone nearly died!

195
00:11:37,002 --> 00:11:38,304
Right! No! Yeah!

196
00:11:38,320 --> 00:11:41,227
I was thinking of a different incident.
The one on the golf course.

197
00:11:41,616 --> 00:11:42,605
What?

198
00:11:43,382 --> 00:11:45,428
I'm talking about the fire!

199
00:11:45,492 --> 00:11:49,230
Oh yeah, fire sorry! I always
get mixed up between golf and fire.

200
00:11:49,991 --> 00:11:52,315
- Just make sure it's off!
- It is off.

201
00:11:52,420 --> 00:11:53,260
I think.

202
00:11:53,772 --> 00:11:56,477
- Well just make sure it is.
- I will make sure it is.

203
00:11:57,330 --> 00:11:58,945
And if it's already off...

204
00:12:02,272 --> 00:12:04,195
I'll just walk away!

205
00:12:06,948 --> 00:12:11,410
Will he get off my back! What is he?
The soldering iron police?

206
00:12:14,075 --> 00:12:15,897
It's off...

207
00:12:16,062 --> 00:12:18,564
that means I turn it... on...

208
00:12:18,893 --> 00:12:20,882
and just walk away!

209
00:12:51,980 --> 00:12:54,154
Oh four! I mean five!

210
00:12:54,238 --> 00:12:55,423
I mean fire!

211
00:13:05,143 --> 00:13:07,535
Now let's see what we have here.

212
00:13:08,012 --> 00:13:10,192
"Stand upright."

213
00:13:11,639 --> 00:13:13,161
Now, I can't read it...

214
00:13:13,671 --> 00:13:14,826
Oh, not me!

215
00:13:14,997 --> 00:13:17,190
I am a giddy-goat!

216
00:13:18,696 --> 00:13:19,564
Okay.

217
00:13:19,831 --> 00:13:21,732
"Remove safety clip."

218
00:13:23,339 --> 00:13:25,692
- Don't look at my feet!
- Fire?

219
00:13:26,167 --> 00:13:27,414
Fire...

220
00:13:28,233 --> 00:13:28,821
No...?

221
00:13:30,325 --> 00:13:33,132
"Aim nozzle at base of fire."

222
00:13:38,527 --> 00:13:40,681
Oh, that is typical!

223
00:13:42,425 --> 00:13:43,964
Why has it done that?

224
00:13:44,078 --> 00:13:46,089
"Made in Britain"

225
00:13:54,089 --> 00:13:56,654
I'll just put this over here,
with the rest...

226
00:13:56,916 --> 00:13:58,294
of the fire.

227
00:14:04,997 --> 00:14:07,433
011  53... No.

228
00:14:07,641 --> 00:14:09,692
011 huh...

229
00:14:11,231 --> 00:14:14,116
0118 999...

230
00:14:17,033 --> 00:14:18,168
...3!

231
00:14:19,383 --> 00:14:22,189
Hello? Is this the emergency services?

232
00:14:23,169 --> 00:14:25,122
Then which country am I speaking to?

233
00:14:25,427 --> 00:14:27,443
Hello? Hello?

234
00:14:29,367 --> 00:14:30,721
I know! Yep.

235
00:14:32,276 --> 00:14:33,240
O kay.

236
00:14:33,978 --> 00:14:35,414
"Subject:..."

237
00:14:35,619 --> 00:14:36,727
"...fire!"

238
00:14:36,912 --> 00:14:38,840
"Dear Sir", stroke, "Madam."

239
00:14:38,878 --> 00:14:41,049
"I'm writing to inform you of a fire

240
00:14:41,076 --> 00:14:43,089
"which has broken out
at the premises of..."

241
00:14:43,178 --> 00:14:44,550
No. That's too formal.

242
00:14:48,886 --> 00:14:51,184
"Dear Sir", stroke, "Madam."

243
00:14:51,463 --> 00:14:53,777
"Fire!", exclamation mark.

244
00:14:54,072 --> 00:14:56,485
"Fire!", exclamation mark.

245
00:14:56,632 --> 00:14:59,136
"Help me!', exclamation mark.

246
00:14:59,265 --> 00:15:01,624
"123, Carendon road."

247
00:15:01,803 --> 00:15:04,111
"Looking forward to hearing from you."

248
00:15:04,383 --> 00:15:07,116
"All the best, Maurice Moss."

249
00:15:10,834 --> 00:15:15,075
What are you wearing size 5 shoes for,
woman? You're at least an 8!

250
00:15:15,110 --> 00:15:15,942
Eight?

251
00:15:16,001 --> 00:15:17,204
How dare you?!

252
00:15:17,256 --> 00:15:19,961
That foot is completely mangled.

253
00:15:20,367 --> 00:15:22,418
And the other one's even worse.

254
00:15:22,624 --> 00:15:24,384
You're not wearing these again.

255
00:15:24,439 --> 00:15:26,724
But I threw away my other shoes,

256
00:15:26,980 --> 00:15:28,981
what am I supposed to wear?

257
00:15:40,729 --> 00:15:43,139
Hello! Hello!

258
00:15:43,293 --> 00:15:46,165
Do you remember me?
Can I have a go in your machine now?

259
00:16:03,672 --> 00:16:06,051
- Oh hello again!
- Oh hi!

260
00:16:10,814 --> 00:16:14,781
Please thank Yamamoto San
for this fine ceremonial sword.

261
00:16:14,951 --> 00:16:17,693
It is a magnificent symbol
of our new merger.

262
00:16:20,675 --> 00:16:22,234
I am sorry that my gift...

263
00:16:22,382 --> 00:16:24,187
a huge pair of Doc' Martins,

264
00:16:24,620 --> 00:16:26,998
is extremely thick and heavy sorts.

265
00:16:27,240 --> 00:16:29,354
It's so poultry in comparison.

266
00:16:32,840 --> 00:16:35,625
Please rest assured
that my cultural advisor will be fired

267
00:16:35,669 --> 00:16:37,549
as soon as this meeting is over.

268
00:16:47,351 --> 00:16:49,374
- These are very heavy shoes.
- Yes.

269
00:16:52,984 --> 00:16:54,892
He feels like... Godzilla!

270
00:16:55,744 --> 00:16:56,484
Does he?

271
00:16:56,520 --> 00:16:57,260
Godzilla!

272
00:16:57,294 --> 00:16:59,126
Go on! Stamp your feet!

273
00:16:59,224 --> 00:17:00,576
Clap him man!

274
00:17:00,668 --> 00:17:01,416
Good!

275
00:17:02,733 --> 00:17:04,241
Oh yeah! Godzilla loves it !

276
00:17:04,283 --> 00:17:06,909
Go on! Break something!

277
00:17:10,538 --> 00:17:12,780
Put your weight into it!

278
00:17:14,731 --> 00:17:17,055
You f...... idiot!!

279
00:17:18,831 --> 00:17:20,314
You stupid old f.......

280
00:17:20,744 --> 00:17:22,375
You f....... J..........

281
00:17:22,582 --> 00:17:25,314
and your big m.... shoes!

282
00:17:25,572 --> 00:17:30,585
Oh you're not! You're nothing!
But I f... ....

283
00:17:56,444 --> 00:18:00,746
- I am... so... sorry, Denholm.
- That was quite a tarring Jen.

284
00:18:01,388 --> 00:18:02,597
It would have been even worse

285
00:18:02,633 --> 00:18:05,201
if Paul hadn't been so quick
on the profanity buzzer.

286
00:18:05,954 --> 00:18:06,952
Well done, Paul!

287
00:18:06,987 --> 00:18:08,731
You're back on the payroll.

288
00:18:09,189 --> 00:18:10,858
As for you... you're fucked up!

289
00:18:11,863 --> 00:18:13,781
Shouting at Japs! Mad feet!

290
00:18:13,890 --> 00:18:15,767
Both classic signs of stress!

291
00:18:15,825 --> 00:18:17,721
And you know how I feel about stress...

292
00:18:18,195 --> 00:18:20,249
Go to your office, and wait for me.

293
00:18:31,012 --> 00:18:34,300
Weirdest thing just happened... Fire!

294
00:18:34,601 --> 00:18:35,786
Fire!

295
00:18:35,924 --> 00:18:37,699
I've sent an e-mail. It's fine!

296
00:18:38,210 --> 00:18:40,465
An e-mail?! It's a fire! Where...

297
00:18:41,383 --> 00:18:43,344
Where is the precinct extinguisher ?

298
00:18:46,896 --> 00:18:50,992
- Made in Britain!
- This is just like the golf incident!

299
00:18:51,060 --> 00:18:52,181
You mean the fire incident?

300
00:18:52,197 --> 00:18:54,492
I mean the fire incident!
Of course I mean the fire incident!

301
00:18:54,516 --> 00:18:55,504
What have you got there?

302
00:18:55,538 --> 00:18:57,016
Oh I'm in trouble! I'm in real trouble!

303
00:18:57,044 --> 00:18:58,959
Help me get my shoes on!
I gotta get my shoes on!

304
00:18:59,000 --> 00:19:00,747
Help me get my shoes on
before Denholm comes!

305
00:19:00,775 --> 00:19:01,933
- Denholm's coming?
- Yeah.

306
00:19:01,944 --> 00:19:03,681
And he's got a... fire! Fire! Fire!

307
00:19:04,587 --> 00:19:07,063
I've taken care of it.

308
00:19:07,077 --> 00:19:09,523
I have to say I feel a little bit
insulted

309
00:19:09,541 --> 00:19:11,489
by the lack of faith you both display.

310
00:19:11,512 --> 00:19:13,176
What happened to your feet?

311
00:19:13,202 --> 00:19:13,993
What is this?

312
00:19:14,027 --> 00:19:15,637
I think a fire in the office

313
00:19:15,683 --> 00:19:18,292
demands a little more attention
than my feet, don't you?

314
00:19:18,309 --> 00:19:20,356
How... how did you get
the stress machine Roy?

315
00:19:20,444 --> 00:19:22,366
I... don't really know.

316
00:19:22,483 --> 00:19:25,432
Met Dr. Holmes by the lift.

317
00:19:25,509 --> 00:19:27,818
Wasn't he nice?
I thought he was a lovely man.

318
00:19:27,843 --> 00:19:29,452
Yes I thought he was a delight.

319
00:19:29,536 --> 00:19:34,639
But I met him there by the lift,
and he just went... mental!

320
00:19:35,652 --> 00:19:39,746
And one thing led to another
and I just... stole it!

321
00:19:40,156 --> 00:19:42,455
- You stole it...
- Yeah!

322
00:19:43,058 --> 00:19:44,775
But that's stealing!

323
00:19:45,596 --> 00:19:46,403
Yeah!

324
00:19:46,569 --> 00:19:49,624
Didn't know what a stress machine was
this morning and now we have 2 of them.

325
00:19:49,682 --> 00:19:51,860
I hate to remind everyone but

326
00:19:51,982 --> 00:19:53,113
I've just destroyed a merger

327
00:19:53,114 --> 00:19:55,093
that probably took hundreds of years
to set up,

328
00:19:55,189 --> 00:19:57,687
the office is on fire,
Denholm is furious, so

329
00:19:57,715 --> 00:19:59,750
could we please concentrate
on what's important,

330
00:19:59,930 --> 00:20:02,178
and help me out with my shoes!

331
00:20:02,278 --> 00:20:03,870
That could get you into trouble!

332
00:20:04,788 --> 00:20:05,894
Empty!

333
00:20:07,617 --> 00:20:08,944
Got a little bit there!

334
00:20:09,634 --> 00:20:12,050
Oh quick! No, no! Quick, no!

335
00:20:12,083 --> 00:20:13,714
These are a size 5...

336
00:20:13,849 --> 00:20:15,587
What are you, eight and a half?

337
00:20:15,651 --> 00:20:16,840
Shut up!

338
00:20:17,012 --> 00:20:18,562
- Come on!
- I'm pushing!

339
00:20:18,599 --> 00:20:20,418
Push it! Oh don't let me push you away!

340
00:20:20,425 --> 00:20:22,322
- Is it in?
- Come on!

341
00:20:22,366 --> 00:20:24,030
Push hard guy!

342
00:20:26,563 --> 00:20:28,293
The heel went right through!

343
00:20:28,309 --> 00:20:30,535
Oh my God it hurts so much!

344
00:20:31,620 --> 00:20:34,099
- Oh now, I'm bleeding!
- Jen!

345
00:20:34,771 --> 00:20:36,400
- Where are you Jen?
- It's Denholm!

346
00:20:36,508 --> 00:20:37,568
What?

347
00:20:38,432 --> 00:20:40,270
Hide the stress machine!

348
00:20:40,353 --> 00:20:42,903
The stress machine?
What about the fire?!

349
00:20:42,943 --> 00:20:45,168
Ok! Put the stress machine
in Jen's room!

350
00:20:45,183 --> 00:20:47,122
- He'll never go in there!
- But what if he does?

351
00:20:47,137 --> 00:20:48,851
It doesn't matter!
He won't know what it is!

352
00:20:48,916 --> 00:20:50,644
I can't go to prison Roy!

353
00:20:50,723 --> 00:20:52,471
They'll rape the flip out of me!

354
00:20:56,374 --> 00:20:58,102
You! Stall him by the door!

355
00:20:58,134 --> 00:20:59,683
I stall... How?

356
00:20:59,745 --> 00:21:01,421
I don't know! Use your womanly ways!

357
00:21:01,488 --> 00:21:04,167
Ok Moss! Pass me that monitor screen.
The broken one.

358
00:21:07,237 --> 00:21:08,264
There you are!

359
00:21:08,931 --> 00:21:09,774
Hello there!

360
00:21:09,893 --> 00:21:12,193
It's no use being womanly with me Jen!

361
00:21:12,521 --> 00:21:13,954
You're in big trouble!

362
00:21:15,861 --> 00:21:16,782
Nice screensaver!

363
00:21:20,070 --> 00:21:21,071
Thank you.

364
00:21:25,375 --> 00:21:26,654
A stress machine!

365
00:21:27,120 --> 00:21:28,282
Just  what we need!

366
00:21:28,841 --> 00:21:30,210
Roll up your sleeves, Jen.

367
00:21:30,386 --> 00:21:31,582
Let's get this over with.

368
00:21:34,188 --> 00:21:36,787
It's a one way ticket
to slammer town for us Roy.

369
00:21:36,842 --> 00:21:38,383
With no return ticket!

370
00:21:41,074 --> 00:21:44,504
I really am losing quite a lot of blood!

371
00:21:44,657 --> 00:21:46,449
You understand this is your last chance?

372
00:21:46,648 --> 00:21:48,070
If the needle goes beyond here...

373
00:21:48,202 --> 00:21:49,474
you will be fired!

374
00:21:50,392 --> 00:21:51,995
Does that make you feel stressed?

375
00:21:52,405 --> 00:21:54,133
Jen! Does it?

376
00:21:54,349 --> 00:21:55,689
No? Are you sure?

377
00:21:55,852 --> 00:21:56,737
Are you sure?

378
00:21:57,110 --> 00:21:58,057
Are you sure?

379
00:21:58,296 --> 00:21:59,128
Are you sure?

380
00:21:59,419 --> 00:22:00,313
Are you sure?

381
00:22:00,649 --> 00:22:01,570
Are you sure?

382
00:22:01,828 --> 00:22:02,764
Are you sure?

383
00:22:03,072 --> 00:22:04,054
Are you sure?

384
00:22:04,234 --> 00:22:05,188
Are you sure?

385
00:22:05,423 --> 00:22:06,556
Are you sure?

386
00:22:06,653 --> 00:22:07,676
Are you sure?

387
00:22:07,883 --> 00:22:08,865
Are you sure?

388
00:22:08,962 --> 00:22:10,482
I'll level with you Roy.

389
00:22:10,552 --> 00:22:13,165
I don't think I'd flourish
in a prison environment.

390
00:22:15,061 --> 00:22:16,854
Could I try on your new glasses?

391
00:22:18,172 --> 00:22:19,554
See this balloon, Jen?

392
00:22:20,024 --> 00:22:21,434
I'm gonna burst it.

393
00:22:21,628 --> 00:22:23,135
But I'm not gonna tell you when...

394
00:22:24,700 --> 00:22:27,075
Do you feel stressed Jen? Jen? Jen?

395
00:22:27,208 --> 00:22:28,124
Look at that!

396
00:22:29,640 --> 00:22:30,985
That's your peace of mind!

397
00:22:31,082 --> 00:22:32,401
And it's gonna go bang!

398
00:22:33,533 --> 00:22:35,267
Do you feel stress now, Jen?

399
00:22:35,385 --> 00:22:35,872
Jen!

400
00:22:37,723 --> 00:22:41,110
Jen! Jen! Do you?!

401
00:22:42,685 --> 00:22:43,930
Evidently not.

402
00:22:45,312 --> 00:22:46,885
Well done Jen!

403
00:22:47,230 --> 00:22:49,269
You're not being fired by me...

404
00:22:49,477 --> 00:22:50,946
at this precise moment.

405
00:22:56,095 --> 00:22:59,008
I love the way the smoke
seems to be coming off the top of it.

406
00:23:01,553 --> 00:23:02,914
Just a second...

407
00:23:05,463 --> 00:23:06,824
I'm late for golf!

408
00:23:14,094 --> 00:23:16,321
I wonder why it didn't work...

409
00:23:17,908 --> 00:23:20,271
"Made In Britain"

410
00:23:22,667 --> 00:23:24,960
I don't know if it's the loss of blood

411
00:23:24,981 --> 00:23:28,991
or the melting plastic from the monitor
but I feel great!

412
00:23:31,626 --> 00:23:35,514
It's so ironic but this entire situation
has been quite stressful.

413
00:23:37,307 --> 00:23:38,604
I'll get help...

414
00:23:41,675 --> 00:23:44,295
You sent an e-mail about a fire?

