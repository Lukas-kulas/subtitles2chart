1
00:00:01,122 --> 00:00:06,197
Transcript : Anyone, Sixe.
Synchro & cut. : Anyone, Sixe.

2
00:00:06,297 --> 00:00:11,308
Suggestions, critics & insults ;)
itcrowdsub@gmail.com
www.forom.com

3
00:00:30,759 --> 00:00:33,218
1x06 Aunt Irma Visits

4
00:00:43,994 --> 00:00:45,468
I know you won a party!

5
00:00:45,734 --> 00:00:47,562
But I have to say thank you
to a few people

6
00:00:47,569 --> 00:00:49,900
after what has been
a momentous week for this company.

7
00:00:50,988 --> 00:00:54,105
Project Iccarus has increased
our productivity... 10 folds.

8
00:00:54,581 --> 00:00:57,783
Every department is working
at three times usual speed...

9
00:00:57,812 --> 00:00:59,898
due to the increased interconnectivity

10
00:00:59,980 --> 00:01:01,172
of our...

11
00:01:01,281 --> 00:01:03,216
computer system!

12
00:01:03,602 --> 00:01:05,107
Computers! Computers!

13
00:01:09,416 --> 00:01:12,145
How could I talk about
all that computer stuff,

14
00:01:12,252 --> 00:01:14,920
all... of that computer gobbledegook,

15
00:01:15,010 --> 00:01:17,692
without... saying a word about...

16
00:01:17,789 --> 00:01:18,846
the lawyers!

17
00:01:19,318 --> 00:01:20,608
They cut the red tape!

18
00:01:20,636 --> 00:01:22,888
They cut the red tape!
They cut the red tape!

19
00:01:22,954 --> 00:01:24,762
- Come on!
- Yes!

20
00:01:30,015 --> 00:01:31,716
That's enough!

21
00:01:31,802 --> 00:01:33,595
And when you're talking about computers,

22
00:01:33,681 --> 00:01:36,101
how can you fail to mention...

23
00:01:36,149 --> 00:01:37,432
Accounts!

24
00:01:38,954 --> 00:01:40,329
He's building up to us!

25
00:01:40,567 --> 00:01:41,728
He must be!

26
00:01:42,453 --> 00:01:44,466
And finally, last but not least...

27
00:01:44,705 --> 00:01:45,710
Champagne...

28
00:01:46,792 --> 00:01:50,013
These three people proved,
time and again,

29
00:01:50,505 --> 00:01:52,167
that they have...

30
00:01:52,168 --> 00:01:53,927
the right stuff!

31
00:01:55,292 --> 00:01:57,102
The toilet cleaners!

32
00:01:59,318 --> 00:02:00,787
They said we couldn't do it!

33
00:02:01,311 --> 00:02:03,229
They said outsourcing
our toilet cleaning

34
00:02:03,256 --> 00:02:04,535
would be inefficient!

35
00:02:04,677 --> 00:02:08,030
Derk, Banhead and Doodles
proved them wrong!

36
00:02:08,501 --> 00:02:09,622
Toilets!

37
00:02:09,718 --> 00:02:11,527
That's right Doodles!

38
00:02:15,583 --> 00:02:16,736
Where are you going?

39
00:02:17,103 --> 00:02:18,334
They do this every time.

40
00:02:18,835 --> 00:02:20,353
They never remember us!

41
00:02:20,822 --> 00:02:22,703
But... you did the work!

42
00:02:23,816 --> 00:02:25,774
You did all the work!

43
00:02:26,857 --> 00:02:29,652
So does this mean we're not going
to the "thank you" party?

44
00:02:29,666 --> 00:02:31,605
Of course we're not going
to the "thank you" party!

45
00:02:31,608 --> 00:02:32,211
Why would we go?

46
00:02:32,215 --> 00:02:33,352
We weren't thanked!

47
00:02:35,634 --> 00:02:36,744
Hello? I.T.,

48
00:02:36,753 --> 00:02:38,552
have you tried turning it off
and on again?

49
00:02:39,474 --> 00:02:41,649
Have you tried
sticking it up your arse?

50
00:02:45,292 --> 00:02:47,359
I just realised that was my mother.

51
00:02:48,363 --> 00:02:51,408
God! I was really looking forward
to that, you know.

52
00:02:51,632 --> 00:02:53,019
Fancy to dance.

53
00:02:53,205 --> 00:02:55,438
I'm glad the toilet guys
got the note, though.

54
00:02:55,659 --> 00:02:58,541
They do good work.
Have you been to the toilets on 12th?

55
00:02:58,774 --> 00:03:00,500
It's like going on holiday!

56
00:03:00,668 --> 00:03:02,149
I now try not to go at home, now.

57
00:03:02,260 --> 00:03:03,207
I save it up!

58
00:03:04,802 --> 00:03:08,016
Oh Jesus! Will you please
do something about that poster?

59
00:03:08,049 --> 00:03:09,075
What poster?

60
00:03:10,214 --> 00:03:11,219
Bloody drawer!

61
00:03:11,257 --> 00:03:13,592
That one! The one with the monkey!
It's horrible!

62
00:03:13,820 --> 00:03:15,597
The monkey?
What's wrong with that monkey?

63
00:03:15,673 --> 00:03:18,396
He's not horrible, he's lovely!
He's a lovely little monkey!

64
00:03:19,358 --> 00:03:20,904
What's he doing! A goal!

65
00:03:21,851 --> 00:03:23,378
Not that monkey!

66
00:03:23,671 --> 00:03:25,069
That monkey...

67
00:03:26,808 --> 00:03:28,537
Oh right. That is a bit frightening!

68
00:03:29,969 --> 00:03:31,929
Anyway! I'd better fix this drawer.

69
00:03:32,069 --> 00:03:34,024
Moss! Do you have the hammer?

70
00:03:34,557 --> 00:03:36,376
You're not listening to me!

71
00:03:36,427 --> 00:03:38,918
I hate it when people
don't listen to me!

72
00:03:39,370 --> 00:03:41,694
- Oh God!
- Hey, that much!

73
00:03:41,765 --> 00:03:43,081
Hyena screeching!

74
00:03:43,095 --> 00:03:44,975
It's like someone's killing a cat
in here!

75
00:03:45,025 --> 00:03:46,578
Some kind of shouty cat!

76
00:03:46,603 --> 00:03:47,980
- What's going on?
- God!

77
00:03:48,017 --> 00:03:49,419
Bring it down! Just bring it down.

78
00:03:49,454 --> 00:03:51,868
I'm so, I'm so sorry.
I'm sorry. I'm just...

79
00:03:52,280 --> 00:03:53,497
I'm just a little bit...

80
00:03:53,623 --> 00:03:54,627
You know...

81
00:03:54,666 --> 00:03:56,242
- You know...
- What?

82
00:03:56,440 --> 00:03:58,297
- What?
- Well, I've got...

83
00:03:58,834 --> 00:04:01,140
- ...you know... at the moment so...
- What?

84
00:04:02,407 --> 00:04:04,932
I've got Aunt Irma visiting.

85
00:04:06,423 --> 00:04:08,342
I Do not like aunt Irma... I've got...

86
00:04:08,945 --> 00:04:10,604
I've got an aunt like that!

87
00:04:10,783 --> 00:04:12,290
It's my term for...

88
00:04:13,061 --> 00:04:15,027
my time of the month...

89
00:04:16,562 --> 00:04:18,204
What time of the month?
The week end?

90
00:04:18,407 --> 00:04:19,310
No.

91
00:04:19,328 --> 00:04:21,254
Does Aunt Irma visit on the week end?

92
00:04:21,290 --> 00:04:22,303
- Moss!
- You know, it...

93
00:04:22,319 --> 00:04:23,881
it's high tide...

94
00:04:24,053 --> 00:04:25,728
-  But we're not on the coast.
- Moss!

95
00:04:25,805 --> 00:04:27,794
I'm closed for maintenance.

96
00:04:27,863 --> 00:04:29,595
- Closed for maintenance?
- Moss!

97
00:04:29,672 --> 00:04:31,939
I've fallen to the Communists!

98
00:04:33,559 --> 00:04:35,568
Well, they do have
some strong arguments.

99
00:04:36,681 --> 00:04:39,978
"Carrie", Moss,
first scene in "Carrie".

100
00:04:51,350 --> 00:04:52,644
Yeah. You know, I...

101
00:04:52,903 --> 00:04:55,564
People tell me I'm not great
during this time, so...

102
00:04:55,597 --> 00:04:57,112
I felt I should warn you.

103
00:04:57,291 --> 00:04:58,672
Right. "Not great" what way?

104
00:04:58,796 --> 00:05:02,262
Just... I'm really...
not great to be around, so

105
00:05:02,457 --> 00:05:04,492
- try not to get on my nerves.
- Oh yeah.

106
00:05:04,516 --> 00:05:07,226
Hey, I've had a few girlfriends.
I'm not completely oblivious!

107
00:05:07,319 --> 00:05:09,351
- You won't even know I'm here.
- Okay.

108
00:05:11,571 --> 00:05:14,896
Oh! This is working right...
The afternoon is so great!

109
00:05:15,104 --> 00:05:17,528
Look at that! So smooth!

110
00:05:17,605 --> 00:05:19,119
Stop doing that!

111
00:05:20,045 --> 00:05:21,051
Oh my God!

112
00:05:21,122 --> 00:05:23,433
Sorry! Sorry, but you see what I mean!

113
00:05:23,453 --> 00:05:24,623
You have to be careful.

114
00:05:24,721 --> 00:05:25,910
Yeah! I'll be careful!

115
00:05:27,982 --> 00:05:29,855
- Has she gone yet?
- Yeah.

116
00:05:30,448 --> 00:05:31,919
What  was all that about?

117
00:05:32,169 --> 00:05:34,180
Goddamme! Ladies!

118
00:05:34,449 --> 00:05:37,453
God bless them!
What would we do without though?

119
00:05:37,609 --> 00:05:39,314
With their ways...

120
00:05:39,516 --> 00:05:42,057
Their mysterious seasons!

121
00:05:42,523 --> 00:05:43,503
The moon!

122
00:05:43,873 --> 00:05:45,985
Glenn Close! Sheila Riston!

123
00:05:46,012 --> 00:05:48,811
All the different kinds of women.

124
00:05:49,296 --> 00:05:50,375
Smashing!

125
00:05:52,224 --> 00:05:54,901
Oh my appointment.
I've got to go and see the psychiatrist.

126
00:05:54,956 --> 00:05:57,445
I can't believe there's a psychiatrist
in the building.

127
00:05:57,517 --> 00:05:59,239
All because those two from accounts

128
00:05:59,258 --> 00:06:02,198
just had enough of everything
and wanted to go to the seaside.

129
00:06:02,261 --> 00:06:03,340
The seaside?

130
00:06:03,518 --> 00:06:05,254
They committed suicide, Roy!

131
00:06:08,102 --> 00:06:10,021
That's right! Yes, of course.

132
00:06:10,149 --> 00:06:11,722
Why do you have to go and see her?

133
00:06:12,194 --> 00:06:13,896
Because I said Jen was dead, that time.

134
00:06:13,975 --> 00:06:15,812
Oh yeah. Yeah. That'll be it.

135
00:06:15,915 --> 00:06:20,231
She said it was something only
a severely disturbed person would do.

136
00:06:20,482 --> 00:06:23,314
And that now, I have to spend
a lot of time in therapy.

137
00:06:23,522 --> 00:06:24,513
Isn't that great?

138
00:06:24,681 --> 00:06:26,859
Oh well, yes, that's...
That's clearly terrific.

139
00:06:26,953 --> 00:06:28,019
No! No, that's bad!

140
00:06:28,082 --> 00:06:29,696
Why do you think that's great?

141
00:06:29,755 --> 00:06:33,480
The psychiatrist is a very...
attractive lady?

142
00:06:33,789 --> 00:06:38,296
- I see... She's of the female...
- Genre? Yes.

143
00:06:38,351 --> 00:06:40,796
A Dr. Melfi to your Tony Soprano?

144
00:06:40,841 --> 00:06:42,866
She's a glamorous old woman.

145
00:06:42,892 --> 00:06:45,307
Oh, the things you could learn
from her...

146
00:06:45,386 --> 00:06:46,665
Or from any woman.

147
00:06:48,438 --> 00:06:49,392
Is Jen is her office?

148
00:06:49,404 --> 00:06:51,422
- I need to get her to sign something.
- Yeah.

149
00:06:53,042 --> 00:06:54,490
Don't get on her nerves.

150
00:06:54,762 --> 00:06:56,304
How would I get on her nerves?

151
00:07:06,452 --> 00:07:07,522
Hello!

152
00:07:18,061 --> 00:07:20,092
Shouldn't I be on the couch?

153
00:07:20,755 --> 00:07:22,329
No, Maurice. Not today.

154
00:07:22,778 --> 00:07:24,198
I've got some good news.

155
00:07:24,414 --> 00:07:25,783
I think you've downed the corner.

156
00:07:26,073 --> 00:07:27,433
I'm not going on the couch?

157
00:07:27,794 --> 00:07:29,471
You don't need me any more, Moss.

158
00:07:29,592 --> 00:07:30,462
What?

159
00:07:30,533 --> 00:07:32,564
Well, there's obviously
nothing wrong with you.

160
00:07:32,769 --> 00:07:35,522
And I think you're ready now
to do without our sessions.

161
00:07:39,052 --> 00:07:40,890
But I've been feeling depressed!

162
00:07:41,844 --> 00:07:43,777
Oh really? Why?

163
00:07:47,470 --> 00:07:49,001
Because I'm pregnant.

164
00:07:49,734 --> 00:07:50,662
What?

165
00:07:50,739 --> 00:07:53,455
I mean...
I had a dream that I was pregnant.

166
00:07:55,372 --> 00:07:56,376
Well...

167
00:07:56,553 --> 00:07:58,574
There's nothing unusual about that.

168
00:07:58,721 --> 00:08:00,160
But when I have the baby,

169
00:08:00,215 --> 00:08:02,858
I looked down
and saw that it was my father!

170
00:08:06,524 --> 00:08:08,901
- That's normal, Moss.
- Really?

171
00:08:09,012 --> 00:08:10,880
That's the maddest thing
I could think of!

172
00:08:11,556 --> 00:08:13,602
My mother? What if it had two heads?

173
00:08:14,150 --> 00:08:15,997
Moss!  Listen, I think you're fine

174
00:08:16,033 --> 00:08:18,520
and there are other people here
who need me more.

175
00:08:18,634 --> 00:08:19,561
But...

176
00:08:19,619 --> 00:08:21,479
I've been having dark thoughts.

177
00:08:22,814 --> 00:08:23,926
Dark thoughts?

178
00:08:24,064 --> 00:08:26,566
Yes. Thoughts about dark things...

179
00:08:26,954 --> 00:08:28,598
What sort of dark things?

180
00:08:28,881 --> 00:08:30,497
Things that are dark...

181
00:08:32,143 --> 00:08:33,220
Like what?

182
00:08:33,759 --> 00:08:36,331
Darkness... night...

183
00:08:37,029 --> 00:08:38,517
Things of the night...

184
00:08:40,065 --> 00:08:41,116
Dracula...

185
00:08:43,341 --> 00:08:45,783
You haven't been thinking
about self-harm?

186
00:08:45,854 --> 00:08:47,686
Self-harm, yes! Harm!

187
00:08:48,946 --> 00:08:50,042
Harm with the self.

188
00:08:51,780 --> 00:08:52,895
Suicide...

189
00:08:52,969 --> 00:08:54,718
For starters, yes.

190
00:08:55,958 --> 00:08:56,982
Moss,

191
00:08:57,098 --> 00:08:59,940
I do not think that you are suicidal.

192
00:09:00,399 --> 00:09:02,067
Why? Why not? I tell you,

193
00:09:02,126 --> 00:09:03,795
I'm at the end of my flipping tether!

194
00:09:03,853 --> 00:09:07,488
Moss! It's not like you
to use that sort of language!

195
00:09:07,556 --> 00:09:09,033
- Flip off!
- Moss!

196
00:09:31,177 --> 00:09:33,317
For God's sake!

197
00:09:36,051 --> 00:09:38,263
- Yes!
- Roy!

198
00:09:38,380 --> 00:09:40,550
Sorry Roy! I may be speaking louder
at the moment

199
00:09:40,571 --> 00:09:42,501
because I'm wearing ear-plugs.

200
00:09:43,608 --> 00:09:45,262
Why are you wearing ear-plugs?

201
00:09:45,305 --> 00:09:46,341
That's right!

202
00:09:48,420 --> 00:09:50,135
- What?
- What's this contraption?

203
00:09:52,553 --> 00:09:55,998
I'm... I'm stealing food
from this machine.

204
00:09:57,267 --> 00:09:58,888
- Yeah!
- You know I do!

205
00:10:00,449 --> 00:10:03,328
Oh, by the way, Roy, your work
on Project Iccarus was very good.

206
00:10:03,355 --> 00:10:05,193
Well done, you did a great job! Thanks!

207
00:10:05,903 --> 00:10:07,502
Yep? I can't hear you!

208
00:10:23,138 --> 00:10:24,671
What's wrong with you?

209
00:10:25,220 --> 00:10:26,601
Oh my God, you're crying?

210
00:10:26,615 --> 00:10:28,128
I'm not crying! You're crying!

211
00:10:28,435 --> 00:10:30,480
- What's wrong?
- Nothing's wrong!

212
00:10:30,485 --> 00:10:33,114
I'm fine! Will you stop getting at me?
You're always getting at me!

213
00:10:33,153 --> 00:10:36,090
I'm not getting at you!
Stop shouting at me!

214
00:10:36,441 --> 00:10:39,031
Why are these things
always so hard to open?!

215
00:10:40,600 --> 00:10:42,340
Oh my God!

216
00:10:42,978 --> 00:10:45,516
Wow, calm down, everybody!

217
00:10:47,898 --> 00:10:50,247
What on Earth is going on?!

218
00:10:50,369 --> 00:10:52,003
I don't know, I feel weird.

219
00:10:52,093 --> 00:10:54,968
I've... I've been swearing
like a flipping docker!

220
00:10:56,862 --> 00:10:59,005
Denholm's just thanked me
for Project Iccarus

221
00:10:59,035 --> 00:11:01,140
and I started crying like an actress!

222
00:11:01,514 --> 00:11:02,723
What's going on?!

223
00:11:03,011 --> 00:11:06,363
Maybe it's all this stuff
that you both eat.

224
00:11:06,420 --> 00:11:08,725
- Oh will you get off that?
- No, honestly, it's true!

225
00:11:08,764 --> 00:11:11,069
Okay, Moss, what did you have
for breakfast this morning?

226
00:11:11,127 --> 00:11:13,118
- Smarties cereals.
- Oh my God!

227
00:11:13,480 --> 00:11:16,440
I  didn't even know
Smarties made a cereal.

228
00:11:16,562 --> 00:11:18,874
They don't.
It's just Smarties in a bowl with milk.

229
00:11:20,814 --> 00:11:22,137
Case closed!

230
00:11:22,176 --> 00:11:24,328
It's too much sugar, that's all!

231
00:11:24,497 --> 00:11:25,982
No no! Hold on now!

232
00:11:25,992 --> 00:11:27,750
I basically live on sugar

233
00:11:27,808 --> 00:11:29,971
and we've never had
these problems before.

234
00:11:31,031 --> 00:11:33,236
The only other explanation is...

235
00:11:34,715 --> 00:11:36,009
No,  no. It's silly.

236
00:11:36,161 --> 00:11:37,165
What?

237
00:11:37,880 --> 00:11:38,891
Well...

238
00:11:39,599 --> 00:11:42,236
I feel ridiculous even saying it but...

239
00:11:42,758 --> 00:11:44,025
Where I worked before,

240
00:11:44,064 --> 00:11:46,979
there was me and a few other girls,
Liz and Susan and

241
00:11:47,045 --> 00:11:49,865
whenever Aunt Irma was visiting me,

242
00:11:50,737 --> 00:11:52,112
we all kind of...

243
00:11:53,616 --> 00:11:54,882
synchronized.

244
00:11:56,058 --> 00:11:57,523
What's that got to do with us?

245
00:11:58,274 --> 00:11:59,201
Well...

246
00:11:59,259 --> 00:12:00,836
- You think...
- Well maybe...

247
00:12:00,858 --> 00:12:02,959
- You're not saying...
- I know it sounds crazy.

248
00:12:02,998 --> 00:12:05,742
Wow! You don't think
that Aunt Irma is visiting us?

249
00:12:06,502 --> 00:12:08,029
The symptoms, Roy!

250
00:12:08,067 --> 00:12:10,251
I am a man! He's a man! We're men!

251
00:12:10,289 --> 00:12:13,137
Okay, okay! Tell me how you're feeling?

252
00:12:15,561 --> 00:12:16,759
I feel delicate.

253
00:12:21,017 --> 00:12:23,039
And... annoyed...

254
00:12:23,834 --> 00:12:25,096
And...

255
00:12:26,170 --> 00:12:27,468
I think I'm ugly.

256
00:12:29,861 --> 00:12:31,477
That's her!

257
00:12:31,564 --> 00:12:32,737
This is ridiculous!

258
00:12:32,776 --> 00:12:34,926
I've never heard anything so...

259
00:12:34,971 --> 00:12:36,247
bizarre in my life!

260
00:12:36,285 --> 00:12:38,572
Oh stop shouting at her!
You're always shouting at her

261
00:12:38,592 --> 00:12:40,285
and it's not fair! It's not fair!

262
00:12:52,388 --> 00:12:54,656
You don't believe this theory, do you?

263
00:12:54,742 --> 00:12:56,401
- No!
- No.

264
00:12:56,802 --> 00:12:59,275
- It's non sense, isn't it?
- She's so silly!

265
00:12:59,634 --> 00:13:01,346
- Sugar!
- Yeah...

266
00:13:03,051 --> 00:13:04,181
What?

267
00:13:04,239 --> 00:13:05,746
That's not what's causing it.

268
00:13:05,842 --> 00:13:07,135
You think it's Irma?

269
00:13:07,481 --> 00:13:08,641
Yes. Don't you?

270
00:13:08,718 --> 00:13:09,792
No!

271
00:13:10,640 --> 00:13:12,325
I'm a man! We're men!

272
00:13:12,476 --> 00:13:13,297
Well, if it's true,

273
00:13:13,336 --> 00:13:15,389
it's the scientific breakthrough
of the afternoon.

274
00:13:16,521 --> 00:13:17,680
It's not true!

275
00:13:17,728 --> 00:13:19,418
Don't worry,
I wasn't about to look foolish

276
00:13:19,446 --> 00:13:21,633
by jumping to conclusions,
so I've sent an e-mail out

277
00:13:21,654 --> 00:13:22,999
asking everyone's opinions.

278
00:13:23,847 --> 00:13:25,478
Right. Well, that's a good idea.

279
00:13:26,460 --> 00:13:28,395
No, sorry.
Could you run that part to me again?

280
00:13:29,138 --> 00:13:30,906
I've sent it to everyone
in my address book.

281
00:13:30,964 --> 00:13:32,046
You should have one.

282
00:13:32,294 --> 00:13:33,286
What?

283
00:13:34,341 --> 00:13:36,525
Do we have PMT?

284
00:13:37,465 --> 00:13:39,055
You sent this out?!

285
00:13:39,116 --> 00:13:40,539
Oh you signed it from both of us!

286
00:13:40,574 --> 00:13:42,188
You signed it from both of us!

287
00:13:43,802 --> 00:13:46,025
Yes. Along with a list of symptoms.

288
00:13:46,157 --> 00:13:47,990
"Headachy", "Weakened",

289
00:13:48,002 --> 00:13:49,854
"Irritability, anxiety!"

290
00:13:50,335 --> 00:13:51,765
"Breast tenderness!"

291
00:13:51,892 --> 00:13:53,343
I get that, don't you?

292
00:13:53,434 --> 00:13:54,337
Yes!

293
00:13:54,367 --> 00:13:57,060
But you cannot tell
the entire I.T. community

294
00:13:57,081 --> 00:13:58,511
that we have PMT!

295
00:13:58,768 --> 00:14:01,321
Come on! No one would use
that sort of information against us.

296
00:14:01,333 --> 00:14:03,599
Now, wait a second!
They would, wouldn't they? Oh no!

297
00:14:05,048 --> 00:14:07,903
There's probably a whole website
devoted to us already!

298
00:14:07,920 --> 00:14:09,729
Oh I only posted it about an hour ago.

299
00:14:09,746 --> 00:14:10,958
Google our names!

300
00:14:11,760 --> 00:14:12,810
Oh no!

301
00:14:16,460 --> 00:14:17,743
What do we gotta do?

302
00:14:17,814 --> 00:14:19,019
I don't know!

303
00:14:19,714 --> 00:14:22,163
Come on! It's my turn with the bottle.

304
00:14:31,481 --> 00:14:34,143
God, sorry Richmond, sorry.
I always forget you work here.

305
00:14:35,169 --> 00:14:38,034
Yeah. I seldom normally come out
this far, only

306
00:14:38,166 --> 00:14:39,961
could you have a word with Moss and Roy?

307
00:14:40,222 --> 00:14:43,008
Ever since they got famous,
they've been at each other's throats.

308
00:14:43,198 --> 00:14:46,100
- Famous?
- Yes. Google their names.

309
00:14:49,665 --> 00:14:52,171
Oh my God!
How many entries do they have?

310
00:14:52,320 --> 00:14:53,478
Thousands...

311
00:14:53,627 --> 00:14:56,123
Mosse's e-mail's spreading
like a virus!

312
00:14:56,329 --> 00:14:59,065
You've unleashed a monster
with that aunt Irma business.

313
00:14:59,154 --> 00:15:03,641
Yes Richmond. Tell me,
how come Aunt Irma isn't affecting you?

314
00:15:04,010 --> 00:15:06,674
Of course it is!
I've been feeling very gloomy all day!

315
00:15:08,189 --> 00:15:10,754
Not my usual cheery self at all!

316
00:15:10,871 --> 00:15:12,604
Yeah. I guess it's
just difficult to tell

317
00:15:12,605 --> 00:15:13,800
underneath all that make-up.

318
00:15:14,689 --> 00:15:16,055
I'm not wearing make-up!

319
00:15:17,940 --> 00:15:19,099
Could you have a word?

320
00:15:19,118 --> 00:15:21,618
They're driving me out
of my tiny little mind.

321
00:15:26,463 --> 00:15:28,147
Guys! You gonna have to...

322
00:15:28,186 --> 00:15:30,323
Oh my goodness!
What happened to your face?

323
00:15:31,549 --> 00:15:33,182
We had an argument.

324
00:15:37,676 --> 00:15:39,993
Oh God, have you seen this?
What's happening?

325
00:15:40,054 --> 00:15:41,878
<i>The rioting has been going</i>

326
00:15:41,879 --> 00:15:43,183
<i>on all night.</i>

327
00:15:43,279 --> 00:15:46,158
<i>The men involved are young, angry and</i>

328
00:15:46,173 --> 00:15:48,916
<i>almost all of them work with computers.</i>

329
00:15:50,433 --> 00:15:52,911
This is insane! What's going on?!

330
00:15:52,975 --> 00:15:55,641
<i>In Tokyo, two games designers</i>

331
00:15:55,660 --> 00:15:58,184
<i>went on the rampage in a shopping centre</i>

332
00:15:58,197 --> 00:16:00,004
<i>and frightened a dog!</i>

333
00:16:00,479 --> 00:16:03,465
<i>In Hamburg, a group
of software developers</i>

334
00:16:03,491 --> 00:16:05,100
<i>shouted at a bus.</i>

335
00:16:05,712 --> 00:16:08,698
<i>The situation is completely
out of cont...</i>

336
00:16:09,021 --> 00:16:11,092
And it's all because of Aunt Irma.

337
00:16:11,162 --> 00:16:12,200
But how?

338
00:16:12,351 --> 00:16:15,473
You shouldn't have sent out this e-mail
detailing the symptoms.

339
00:16:15,540 --> 00:16:18,550
You know how suggestible
and easily swayed I.T. people are...

340
00:16:18,604 --> 00:16:20,425
- That's not true!
- Yes it is!

341
00:16:20,501 --> 00:16:21,896
No you're right. Of course it is.

342
00:16:22,790 --> 00:16:24,484
You shouldn't have sent that e-mail,
Moss!

343
00:16:24,527 --> 00:16:25,511
Oh don't blame me!

344
00:16:25,777 --> 00:16:27,613
- It's your fault!
- Guys!

345
00:16:27,784 --> 00:16:29,661
Guys, come on! You're fighting!

346
00:16:29,694 --> 00:16:31,342
It's like you're in a trance!

347
00:16:31,375 --> 00:16:33,139
No, you're right!
We shouldn't be doing this!

348
00:16:33,148 --> 00:16:34,733
This is enough! We never fight!

349
00:16:35,366 --> 00:16:36,684
We have to do something.

350
00:16:36,961 --> 00:16:37,962
Yeah, whatever.

351
00:16:41,465 --> 00:16:42,547
I'm going in.

352
00:16:42,629 --> 00:16:45,031
Damn it Roy! I never thought
it would come to this!

353
00:16:45,115 --> 00:16:46,539
The hell it has!

354
00:16:46,582 --> 00:16:49,102
She's the only one that's had
direct contact with Irma.

355
00:16:49,305 --> 00:16:50,986
If we can just calm her down then...

356
00:16:51,461 --> 00:16:54,089
We can hope and pray
that everybody else calms down too.

357
00:16:54,818 --> 00:16:56,311
You've got big balls, Roy!

358
00:16:59,906 --> 00:17:00,967
Thanks.

359
00:17:02,196 --> 00:17:03,303
You're very welcome.

360
00:17:07,030 --> 00:17:07,926
Hiya!

361
00:17:08,103 --> 00:17:10,632
Can't even find a bloody pen in here!

362
00:17:11,506 --> 00:17:13,008
How is my soldier?

363
00:17:13,227 --> 00:17:14,361
What do you want?!

364
00:17:14,416 --> 00:17:16,227
I just came in for a little chat.

365
00:17:17,195 --> 00:17:18,744
You  are looking good today!

366
00:17:18,824 --> 00:17:20,771
- Ah, thank you.
- So slim!

367
00:17:21,002 --> 00:17:22,853
Yeah, I'm trying to stay in shape,
you know...

368
00:17:22,931 --> 00:17:24,700
Oh your hair looks great!

369
00:17:24,767 --> 00:17:26,039
Yeah. Hair conditioner.

370
00:17:27,146 --> 00:17:28,955
The action on this is great!

371
00:17:32,590 --> 00:17:33,575
Hello!

372
00:17:34,296 --> 00:17:35,463
Hello Maurice.

373
00:17:36,040 --> 00:17:38,377
I have to tell you
I've a little confession to make.

374
00:17:38,819 --> 00:17:41,447
As you may or may not know,
for ethical reasons,

375
00:17:41,489 --> 00:17:43,218
I'm required not to have any...

376
00:17:43,383 --> 00:17:45,955
relations with any of my patients.

377
00:17:49,929 --> 00:17:51,572
But for the last few months,

378
00:17:52,189 --> 00:17:54,131
I've been developing some

379
00:17:55,101 --> 00:17:56,183
warm...

380
00:17:56,293 --> 00:17:58,022
feelings towards you.

381
00:17:58,600 --> 00:18:01,488
And the real reason
I terminated your therapy

382
00:18:02,213 --> 00:18:04,246
was so that I could explore...

383
00:18:05,117 --> 00:18:06,900
some of these feelings...

384
00:18:11,320 --> 00:18:12,535
I see...

385
00:18:15,884 --> 00:18:16,861
Jen,

386
00:18:17,193 --> 00:18:18,727
I was just wondering,

387
00:18:19,267 --> 00:18:21,875
when you were working
in your old job, with the girls,

388
00:18:21,989 --> 00:18:24,343
if things ever got on top of you,

389
00:18:24,718 --> 00:18:26,979
what would you do to...

390
00:18:28,880 --> 00:18:30,327
relax?

391
00:18:31,741 --> 00:18:33,545
We'd have a big girls' night out.

392
00:18:34,601 --> 00:18:36,053
I'd love that actually.

393
00:18:36,733 --> 00:18:38,645
A big girly night out!

394
00:18:39,375 --> 00:18:41,185
There aren't chances of that round here.

395
00:18:41,400 --> 00:18:42,540
Not necessarily.

396
00:18:48,173 --> 00:18:49,178
Mom?!

397
00:18:55,442 --> 00:18:57,238
This is my psychiatrist.

398
00:18:57,248 --> 00:18:59,124
Oh! No. No, I'm... I'm sorry.

399
00:18:59,142 --> 00:19:00,443
It's just that you...

400
00:19:00,485 --> 00:19:03,338
You look exactly like my mother.

401
00:19:03,687 --> 00:19:05,986
No, no! But that... that...
don't be offended by that.

402
00:19:06,025 --> 00:19:08,529
She's... she's a very sexy woman.

403
00:19:09,547 --> 00:19:12,865
Not not... not that I would want
to have sex with her...

404
00:19:14,282 --> 00:19:16,787
I know that that's what
you psychiatrists think!

405
00:19:17,017 --> 00:19:20,946
But I swear to God
that there's nothing to it in my case!

406
00:19:21,875 --> 00:19:26,238
You know my mother wouldn't stand
for any of that kind of nonsense!

407
00:19:27,465 --> 00:19:29,343
I can just hear her now...

408
00:19:30,081 --> 00:19:31,611
"What are you doing Roy?"

409
00:19:31,911 --> 00:19:33,257
"What are you doing?!"

410
00:19:37,211 --> 00:19:38,741
You handled that well!

411
00:19:39,977 --> 00:19:41,864
That's the psychiatrist!

412
00:19:42,064 --> 00:19:43,165
Yes, I...

413
00:19:43,204 --> 00:19:45,169
I think she's my girlfriend now.

414
00:19:45,912 --> 00:19:47,517
No, Moss! Listen to me.

415
00:19:47,536 --> 00:19:49,320
You can never see that woman again.

416
00:19:49,334 --> 00:19:51,080
She looks exactly like my mother.

417
00:19:51,104 --> 00:19:52,605
Oh stop it. You're exaggerating.

418
00:19:52,640 --> 00:19:53,896
She's the spit of her!

419
00:19:53,983 --> 00:19:55,684
Oh come on. What went on in there?

420
00:19:56,116 --> 00:19:57,547
All right. Okay.

421
00:19:58,556 --> 00:20:00,085
What would you say...

422
00:20:00,220 --> 00:20:02,111
to a big girls' night out?

423
00:20:03,889 --> 00:20:05,174
How big are the girls?

424
00:20:18,577 --> 00:20:22,008
I can't believe I'd never seen
"Steel Magnolias"!

425
00:20:23,752 --> 00:20:25,240
Oh my God! It's good, isn't it?

426
00:20:27,052 --> 00:20:29,485
- I know what we could watch next!
- Yeah!

427
00:20:29,945 --> 00:20:32,723
- Your really liked that film?
- Yes. Didn't you?

428
00:20:32,789 --> 00:20:34,968
No! It was heart-warming!

429
00:20:35,134 --> 00:20:37,070
Well I feel like a princess, Roy!

430
00:20:37,098 --> 00:20:39,152
And don't tell me
you don't feel the same!

431
00:20:41,649 --> 00:20:43,394
Well, my breast-tenderness has gone.

432
00:20:44,822 --> 00:20:47,255
No! What am I talking about?!
I'm a man! We're men!

433
00:20:47,330 --> 00:20:48,644
It's the worst night of my life!

434
00:20:48,702 --> 00:20:49,990
Hiya!

435
00:20:50,125 --> 00:20:51,127
Okay!

436
00:20:51,877 --> 00:20:55,116
Beaches... or... Dirty Dancing!

437
00:20:55,163 --> 00:20:57,307
Oh, Beaches! No! Dirty Dancing!

438
00:20:57,351 --> 00:20:58,896
- Oh I can't decide!
- Do!

439
00:20:58,922 --> 00:21:00,458
God! Dear God!

440
00:21:00,477 --> 00:21:02,317
I don't know which one to watch either!

441
00:21:03,306 --> 00:21:05,018
Oh hey! But you know what we should do?

442
00:21:05,043 --> 00:21:07,118
- What?
- Something else!

443
00:21:08,189 --> 00:21:09,450
Oh! I know!

444
00:21:09,504 --> 00:21:12,053
Why don't we go to the thank you party?

445
00:21:12,069 --> 00:21:13,712
But I thought you didn't want to go.

446
00:21:14,048 --> 00:21:15,284
Well, you know...

447
00:21:15,701 --> 00:21:17,796
Everybody else is there,
and they did nothing

448
00:21:17,852 --> 00:21:20,082
- and we did all the work!
- Yeah...

449
00:21:20,225 --> 00:21:22,682
- Screw them!
- Yeah! Screw them!

450
00:21:23,373 --> 00:21:25,440
- Ok, come on! Let's go girls!
- Yeah!

451
00:21:26,065 --> 00:21:27,077
Ok but first,

452
00:21:27,095 --> 00:21:28,468
10 minutes of Beaches!

453
00:21:39,480 --> 00:21:42,319
<i>www.ladiesproblem.com</i>

454
00:22:07,071 --> 00:22:08,271
Oh Lord!

455
00:22:10,100 --> 00:22:13,604
Oh yes, I suggest
I ask Dr. Mendall to join us.

456
00:22:13,943 --> 00:22:14,926
What?

457
00:22:15,003 --> 00:22:16,888
Moss! Over here!

458
00:22:22,394 --> 00:22:23,603
What shall we do?

459
00:22:26,076 --> 00:22:27,080
Nothing...

460
00:22:27,378 --> 00:22:29,272
I just... I really need to get drunk.

461
00:22:29,349 --> 00:22:31,363
Yeah, I'd like to get drunk.

462
00:22:33,795 --> 00:22:36,210
- We should get drunk together then.
- Okay, yeah.

463
00:22:37,911 --> 00:22:40,975
We should get so drunk
that we don't know what's going on.

464
00:22:42,232 --> 00:22:43,272
All right.

465
00:22:48,469 --> 00:22:52,874
...and, turned out,
4 dead cyclists on my fault!

466
00:22:54,222 --> 00:22:55,675
Brilliant! Same thing I...

467
00:22:55,733 --> 00:22:57,665
I.T. guys! Can we?!

468
00:23:37,829 --> 00:23:39,057
Oh dear.

469
00:23:47,203 --> 00:23:49,830
Oh my God! I didn't!

470
00:23:50,854 --> 00:23:52,614
I didn't!

471
00:23:55,546 --> 00:23:56,976
Good morning!

472
00:24:05,949 --> 00:24:07,661
I've made you a cup of tea.

473
00:24:08,281 --> 00:24:09,584
Ah, thanks mom.

474
00:24:09,984 --> 00:24:10,954
Mom?

475
00:24:11,543 --> 00:24:13,010
That's interesting!

476
00:24:18,251 --> 00:24:21,591
To be continued.

