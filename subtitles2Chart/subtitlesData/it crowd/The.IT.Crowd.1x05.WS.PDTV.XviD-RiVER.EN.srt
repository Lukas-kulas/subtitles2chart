1
00:00:01,127 --> 00:00:04,779
Transcript : Sixe, Anyone.
Synchro & cut. : Sixe, Anyone.

2
00:00:04,826 --> 00:00:08,366
Suggestions, critics & insults ;)
itcrowdsub@gmail.com
www.forom.com

3
00:00:30,769 --> 00:00:34,093
1x05 - The Haunting of Bill Crouse

4
00:00:34,686 --> 00:00:35,490
OK.

5
00:00:39,774 --> 00:00:40,853
Here we go.

6
00:00:42,313 --> 00:00:43,751
Sit properly?

7
00:00:45,751 --> 00:00:46,849
Cheers.

8
00:00:48,237 --> 00:00:49,178
Bill?

9
00:00:50,285 --> 00:00:51,492
Mick Hucknall.

10
00:00:56,606 --> 00:00:57,626
Hey, this is fun.

11
00:00:58,311 --> 00:01:01,176
- It's been a good night so far.
- Yes, it's been really good, yeah.

12
00:01:01,332 --> 00:01:03,356
Yeah, I'll be honest,
when Julie suggested

13
00:01:03,356 --> 00:01:04,864
we get together, I was like no.

14
00:01:04,864 --> 00:01:06,331
you know, I get set up a lot

15
00:01:06,484 --> 00:01:09,294
by friends, and it never works out.

16
00:01:09,439 --> 00:01:10,578
Tell me about it. Me too.

17
00:01:10,611 --> 00:01:12,582
You know, I hate dates usually.

18
00:01:12,729 --> 00:01:15,925
They should call dates
"Spend some time with a big fat loser!"

19
00:01:18,518 --> 00:01:19,886
But this has been brilliant.

20
00:01:20,004 --> 00:01:21,023
Hey...

21
00:01:21,200 --> 00:01:22,808
I was thinking, afterwards,

22
00:01:23,063 --> 00:01:24,308
maybe you'd like to

23
00:01:24,406 --> 00:01:25,641
come back to my place?

24
00:01:25,689 --> 00:01:27,642
We could listen to the new
Jamie Cullum album.

25
00:01:27,681 --> 00:01:28,837
Maybe, yeah!

26
00:01:28,945 --> 00:01:31,043
Yeah, yeah, that'd be...
really nice actually.

27
00:01:31,161 --> 00:01:32,190
Great.

28
00:01:33,935 --> 00:01:35,484
Let me guess...

29
00:01:35,778 --> 00:01:37,171
You're a...

30
00:01:37,376 --> 00:01:38,729
red wine person?

31
00:01:38,827 --> 00:01:40,033
Yes, yes I am!

32
00:01:40,837 --> 00:01:43,562
I would be too, if we could get
some service round here!

33
00:01:43,689 --> 00:01:44,866
Yeah, you, Hucknall.

34
00:01:45,863 --> 00:01:48,969
When you, when you Simply Ready?

35
00:01:49,933 --> 00:01:51,952
Unbelievable.

36
00:01:52,031 --> 00:01:53,501
Oh, I should say, um,

37
00:01:53,638 --> 00:01:55,050
I don't really like sharing.

38
00:01:55,297 --> 00:01:57,846
So you get what you want,
I'll get what I want

39
00:01:57,944 --> 00:02:00,865
we leave it at that.
I just think it's a lot less confusing.

40
00:02:01,090 --> 00:02:02,110
Sure, sure.

41
00:02:02,146 --> 00:02:03,571
- Did you say Jamie Cullum?
- Yeah.

42
00:02:03,594 --> 00:02:04,894
I hate Jamie Cullum.

43
00:02:06,600 --> 00:02:08,717
Oh, and, just to say,

44
00:02:08,773 --> 00:02:10,831
"I don't really like sharing."

45
00:02:11,204 --> 00:02:13,125
With the rising inflection
and everything.

46
00:02:13,263 --> 00:02:16,753
In a tapas restaurant,
he doesn't like sharing.

47
00:02:17,361 --> 00:02:18,694
What the heck is tapas?

48
00:02:18,812 --> 00:02:19,812
You know, tapas,

49
00:02:19,979 --> 00:02:22,097
- tiny food from Spain.
- Oh, yes.

50
00:02:22,214 --> 00:02:24,704
- Teipas.
- Yeah, that's no how you say it.

51
00:02:25,283 --> 00:02:26,400
Yes it is.

52
00:02:27,067 --> 00:02:28,479
You're a teipas.

53
00:02:29,557 --> 00:02:30,793
What else? What else?

54
00:02:30,920 --> 00:02:31,940
What else?

55
00:02:31,999 --> 00:02:34,097
Oh God, you should have seen him
with the waiters.

56
00:02:34,205 --> 00:02:35,666
- Rude?
- Unbelievable!

57
00:02:35,764 --> 00:02:37,606
Oh yeah, I used to work as a waiter.

58
00:02:37,665 --> 00:02:41,332
If anyone was ever rude to me, I used to
carry their food around in my trousers.

59
00:02:42,230 --> 00:02:44,622
Oh my God! Before you brought it
to their table?

60
00:02:45,779 --> 00:02:46,857
No, after.

61
00:02:47,838 --> 00:02:50,799
Of course before!
Why would I do it after?

62
00:02:50,984 --> 00:02:52,828
Do you think they did that to him?

63
00:02:53,024 --> 00:02:55,181
Okay, well. While he was eating,

64
00:02:55,298 --> 00:02:56,788
did you ear anyone laughing?

65
00:02:56,886 --> 00:02:58,768
Like... in the kitchen area.

66
00:02:59,710 --> 00:03:00,553
Yes!

67
00:03:00,621 --> 00:03:02,229
Yes I did, actually, yes I did.

68
00:03:02,327 --> 00:03:03,611
That'll be trouser food.

69
00:03:03,650 --> 00:03:04,895
- Oh God!
- Yeah.

70
00:03:04,934 --> 00:03:07,026
Probably a good thing
you weren't sharing.

71
00:03:07,190 --> 00:03:08,141
My God!

72
00:03:08,316 --> 00:03:09,728
I could have saved you the trouble.

73
00:03:10,022 --> 00:03:11,904
That guy is famous, he's a mouth.

74
00:03:11,924 --> 00:03:13,159
They call him "The News"

75
00:03:13,218 --> 00:03:15,424
because he was always talking about
who he slept with.

76
00:03:16,522 --> 00:03:19,483
I would honestly rather sleep
with a rat.

77
00:03:19,622 --> 00:03:20,609
Literally.

78
00:03:22,264 --> 00:03:24,343
The thing I cannot understand yet, this:

79
00:03:24,477 --> 00:03:27,110
this is a man Julie on 5th
thinks is perfect for me,

80
00:03:27,145 --> 00:03:28,307
I mean what was she thinking?

81
00:03:28,337 --> 00:03:29,186
Wait a second.

82
00:03:29,244 --> 00:03:30,752
- Who do you know on 5th?
- Why?

83
00:03:30,844 --> 00:03:32,611
What was the name,
was it Judy or Julie?

84
00:03:32,612 --> 00:03:33,720
Why do you need to know?

85
00:03:33,795 --> 00:03:34,731
No reason.

86
00:03:34,780 --> 00:03:36,597
Lots of nice looking girls on fifth.

87
00:03:36,915 --> 00:03:38,326
Looking for an in, aren't we?

88
00:03:38,345 --> 00:03:39,485
An in? No!

89
00:03:39,620 --> 00:03:40,722
Thank you, no.

90
00:03:40,741 --> 00:03:43,640
Don't use my name
to start leching around on 5th.

91
00:03:44,557 --> 00:03:46,799
For your information, I do not lech

92
00:03:46,963 --> 00:03:49,707
I have a little more class than that,
thank you very much.

93
00:03:50,006 --> 00:03:51,668
- Post.
- Thank you.

94
00:03:51,900 --> 00:03:53,234
Oh no, this is wrong.

95
00:03:53,388 --> 00:03:55,978
A lingerie catalogue
for a Cathy Morgan Foreman.

96
00:03:56,045 --> 00:03:57,664
I'll make sure that Cathy gets it.

97
00:03:58,354 --> 00:04:00,526
Cathy Morgan Foreman,
who is Cathy Morgan Foreman?

98
00:04:00,573 --> 00:04:02,316
Cathy works in...

99
00:04:02,335 --> 00:04:05,756
Cathy works
in the department of Shulupust.

100
00:04:05,968 --> 00:04:08,731
- Department of what?
- Oh excuse me, that's my phone ringing.

101
00:04:08,789 --> 00:04:10,953
- I didn't hear anything?
- It's on vibrate.

102
00:04:11,127 --> 00:04:12,054
Hello?

103
00:04:12,141 --> 00:04:13,358
Hello, Cathy!

104
00:04:13,416 --> 00:04:16,527
Yes, you parcel did arrive,
I'm keeping it here safe for you.

105
00:04:16,565 --> 00:04:18,623
No problem at all Cathy Morgan Foreman.

106
00:04:19,599 --> 00:04:21,019
If he comes by...

107
00:04:21,077 --> 00:04:22,352
Right. If who comes by?

108
00:04:24,106 --> 00:04:25,057
The News.

109
00:04:25,194 --> 00:04:26,120
Yes, right?

110
00:04:26,325 --> 00:04:27,318
If he calls,

111
00:04:27,361 --> 00:04:28,524
just tell him something.

112
00:04:29,099 --> 00:04:30,684
- Leave it with me!
- Great!

113
00:04:30,925 --> 00:04:33,050
Although I should just say
I'm not very good at lying.

114
00:04:33,128 --> 00:04:34,345
I'm very good at lying.

115
00:04:34,403 --> 00:04:36,103
I thought you were on the phone!

116
00:04:36,857 --> 00:04:37,958
Yeah, I am.

117
00:04:38,035 --> 00:04:39,871
Yes, that's fine.

118
00:04:43,432 --> 00:04:44,437
Hello?

119
00:04:44,572 --> 00:04:47,993
Listen Moss,
you're not going to Court, yeah?

120
00:04:48,341 --> 00:04:49,867
Just tell him something.

121
00:04:49,945 --> 00:04:52,776
It's just... I've never been very good
at judging the scale

122
00:04:52,872 --> 00:04:54,959
- of the lie...
- Yes, stop over thinking everything.

123
00:04:54,995 --> 00:04:57,878
- I couldn�t do it when I was a child,
- You're getting yourself in a tease,

124
00:04:57,885 --> 00:04:58,932
I told a whopper and...

125
00:04:58,970 --> 00:05:00,209
Just tell him I'm busy!

126
00:05:02,765 --> 00:05:04,292
You're busy!

127
00:05:04,968 --> 00:05:06,456
That's brilliant!

128
00:05:08,281 --> 00:05:10,475
Just don't let him in here
under any circumstances.

129
00:05:10,842 --> 00:05:11,847
No!

130
00:05:11,924 --> 00:05:12,968
You're busy!

131
00:05:14,794 --> 00:05:17,016
Hey, um, what was the name of that girl,

132
00:05:17,132 --> 00:05:20,552
that Jen knows, was it Julie or Judy?

133
00:05:20,726 --> 00:05:22,195
Yes, one of those.

134
00:05:27,615 --> 00:05:30,301
Okay, I'm just going to go
for a letch on fifth.

135
00:05:30,417 --> 00:05:31,460
Yes.

136
00:05:31,808 --> 00:05:33,992
Look! I told you she's busy!

137
00:05:34,204 --> 00:05:37,528
Hey, back off, bastard!
This lady's busy!

138
00:05:37,673 --> 00:05:39,296
Too busy for you.

139
00:05:39,711 --> 00:05:40,813
Busy!

140
00:05:40,967 --> 00:05:42,107
Brilliant!

141
00:06:00,315 --> 00:06:01,977
- Hello?
- Hello!

142
00:06:03,378 --> 00:06:05,020
I'm just looking for Jen.

143
00:06:16,523 --> 00:06:18,649
I'm afraid Jen can't see you
at the moment.

144
00:06:18,721 --> 00:06:20,096
She's very busy.

145
00:06:20,342 --> 00:06:22,517
- Is she?
- She's a busy Miss Lizzy!

146
00:06:22,816 --> 00:06:23,895
Good bye!

147
00:06:27,146 --> 00:06:28,248
What is she doing?

148
00:06:28,364 --> 00:06:29,542
She's doing fine.

149
00:06:30,509 --> 00:06:32,383
Not how is she doing, what is she doing?

150
00:06:34,524 --> 00:06:35,947
- Is she in here?
- No.

151
00:06:35,954 --> 00:06:37,681
I won't keep her,
I want to give her these.

152
00:06:37,690 --> 00:06:39,230
- Don't give her.
- I'll give her these.

153
00:06:39,240 --> 00:06:40,805
- She won't like them.
- She won't mind!

154
00:06:40,809 --> 00:06:41,445
She will mind.

155
00:06:41,446 --> 00:06:43,194
It's not a problem.
I won't take any time.

156
00:06:43,222 --> 00:06:44,438
Look, why can't I go in?

157
00:06:44,480 --> 00:06:45,831
Because she's dead!

158
00:06:51,473 --> 00:06:52,401
What?

159
00:06:52,402 --> 00:06:55,164
Yeah, she's dead. She... she died.

160
00:06:56,459 --> 00:06:57,715
She died last night.

161
00:06:59,338 --> 00:07:00,749
She died?!

162
00:07:00,981 --> 00:07:01,908
Yeah!

163
00:07:02,101 --> 00:07:03,087
Completely.

164
00:07:05,038 --> 00:07:06,391
Oh God!

165
00:07:07,587 --> 00:07:08,650
Fuck!

166
00:07:08,727 --> 00:07:10,080
I just can't believe it!

167
00:07:10,157 --> 00:07:13,249
Well, I'd hardly make up
something like that, would I?

168
00:07:14,061 --> 00:07:16,070
No, no of course, of course not.

169
00:07:16,592 --> 00:07:18,080
Oh God!

170
00:07:19,116 --> 00:07:21,049
Well, she was a bit off
at the restaurant.

171
00:07:23,754 --> 00:07:24,778
I mean...

172
00:07:25,145 --> 00:07:26,150
Was it the food?

173
00:07:26,169 --> 00:07:28,063
- The tapas, do they know?
- Teipas!

174
00:07:28,111 --> 00:07:29,445
I don't know.

175
00:07:30,179 --> 00:07:31,687
Yes it was, why not.

176
00:07:34,574 --> 00:07:35,560
Oh Jesus!

177
00:07:36,405 --> 00:07:37,816
I know it's terrible, but,

178
00:07:38,163 --> 00:07:39,555
thank God, we weren't sharing.

179
00:07:42,666 --> 00:07:44,018
I just can't get over it.

180
00:07:44,192 --> 00:07:45,178
Yeah, well,

181
00:07:45,255 --> 00:07:47,285
we all have to go sometime.

182
00:07:50,637 --> 00:07:52,665
It's terrible, it's just terrible!

183
00:07:52,781 --> 00:07:54,520
It's bloody awful!

184
00:07:56,152 --> 00:07:57,640
She was so beautiful!

185
00:07:58,413 --> 00:08:00,017
She was some piece of ass.

186
00:08:05,762 --> 00:08:08,718
Anyhow, can't mourn
all the ruddy day, so...

187
00:08:14,414 --> 00:08:15,709
You know...

188
00:08:17,410 --> 00:08:19,844
I was the last person to sleep with her.

189
00:08:20,927 --> 00:08:23,342
- Really? She said that...
- What?

190
00:08:24,675 --> 00:08:29,989
She said she was looking forward to sleeping with you.

191
00:08:50,067 --> 00:08:52,386
I can't believe he lied like that.

192
00:08:53,600 --> 00:08:57,909
Doesn't he realise that lying
can lead to very serious ramifications?

193
00:09:02,605 --> 00:09:03,629
Hello?

194
00:09:07,700 --> 00:09:09,014
Hello!

195
00:09:13,381 --> 00:09:14,657
I'm Roy!

196
00:09:15,111 --> 00:09:16,328
I.T.

197
00:09:17,294 --> 00:09:20,965
My boss has assigned me to any work
you might want done...

198
00:09:21,361 --> 00:09:23,003
personally.

199
00:09:23,139 --> 00:09:24,627
personal... personally.

200
00:09:25,033 --> 00:09:27,178
Basically, if you want to

201
00:09:28,645 --> 00:09:29,887
just take my card.

202
00:09:32,258 --> 00:09:33,534
And...

203
00:09:35,028 --> 00:09:36,438
if you need anything done

204
00:09:36,921 --> 00:09:38,197
to your computer,

205
00:09:38,293 --> 00:09:42,834
just give me a buzz, on me phone.
It's my mobile phone, so

206
00:09:43,377 --> 00:09:46,022
you won't have to go through
all that call waiting...

207
00:09:46,147 --> 00:09:47,152
Lover.

208
00:09:47,345 --> 00:09:48,814
Okay, thank you, Roy.

209
00:09:48,939 --> 00:09:50,639
You're welcome, Judy.

210
00:09:51,277 --> 00:09:53,016
Oh, I'm not Judy, I'm Julie.

211
00:09:53,209 --> 00:09:54,446
Judy?

212
00:09:56,716 --> 00:09:58,513
Someone to see you!

213
00:09:59,981 --> 00:10:01,798
This man says you should take his card.

214
00:10:04,126 --> 00:10:06,367
<i>The girl from I.T., that's terrible.</i>

215
00:10:06,502 --> 00:10:08,493
I know, terrible, terrible thing.

216
00:10:09,169 --> 00:10:12,106
You know, I was actually the last person
to sleep with her.

217
00:10:13,613 --> 00:10:15,024
Very sudden, yes.

218
00:10:15,159 --> 00:10:16,975
I heard it was some bad tapas.

219
00:10:17,072 --> 00:10:18,076
Teipas.

220
00:10:18,173 --> 00:10:19,100
Yes.

221
00:10:19,351 --> 00:10:20,356
You must be stunned.

222
00:10:20,433 --> 00:10:21,670
I'm flabbergasted

223
00:10:21,728 --> 00:10:23,834
Well, you know where we are
if you need us.

224
00:10:24,008 --> 00:10:26,018
Sure, I'll be fine, thanks.

225
00:10:26,230 --> 00:10:28,337
Just sort of stay away
from this whole area,

226
00:10:28,471 --> 00:10:31,003
while we come to terms with the loss.

227
00:10:32,259 --> 00:10:34,674
Leave me now, I would be alone.

228
00:10:53,259 --> 00:10:54,303
Good lech?

229
00:10:54,998 --> 00:10:56,003
No!

230
00:10:56,128 --> 00:11:01,470
The girl, oh my God,
the girl, Judy, she's a...

231
00:11:02,301 --> 00:11:04,485
- Have you seen this girl?
- What's she like?

232
00:11:04,657 --> 00:11:07,748
- Well, she has hair on her eyes.
- Who has hair on her eyes?

233
00:11:07,845 --> 00:11:09,236
- Not you.
- No.

234
00:11:10,009 --> 00:11:12,096
I'm going for a coffee,
anyone wants anything?

235
00:11:12,279 --> 00:11:13,806
No? Okay, bye!

236
00:11:36,177 --> 00:11:38,013
- Hello?
- Hello?

237
00:11:39,935 --> 00:11:41,326
Hello Judy?

238
00:11:41,945 --> 00:11:43,375
What can I do for you?

239
00:11:43,394 --> 00:11:44,824
Computer is broken!

240
00:11:44,930 --> 00:11:47,037
Is it a PC or a Mac?

241
00:11:48,003 --> 00:11:49,008
<i>Yes!</i>

242
00:11:50,554 --> 00:11:52,157
Well, what's wrong with it?

243
00:11:52,235 --> 00:11:53,510
Broken down!

244
00:11:53,616 --> 00:11:55,780
Yeah, broken down how, Judy?

245
00:11:55,944 --> 00:11:58,272
- Big bang noise!
- Big bang noise, what...

246
00:11:58,485 --> 00:12:00,533
What's the precise nature
of the problem?

247
00:12:04,117 --> 00:12:05,045
MIO (My Input Output)

248
00:12:05,586 --> 00:12:07,963
Judy, the battery of my phone is...

249
00:12:12,310 --> 00:12:13,914
- Look at this.
- What?

250
00:12:14,069 --> 00:12:15,692
How nice is this?

251
00:12:16,812 --> 00:12:19,460
I just got a big load of flowers,

252
00:12:19,653 --> 00:12:21,179
and a big card!

253
00:12:21,295 --> 00:12:23,923
And everyone has signed it!

254
00:12:24,802 --> 00:12:26,735
"You will always be in our heart!"

255
00:12:27,362 --> 00:12:30,145
My God! And you said
they weren't nice here!

256
00:12:30,618 --> 00:12:31,449
Yeah.

257
00:12:32,917 --> 00:12:35,352
I don't understand
why they've done this?

258
00:12:35,921 --> 00:12:36,888
Why?

259
00:12:36,946 --> 00:12:38,356
I'll tell for why.

260
00:12:38,608 --> 00:12:40,946
It's because everyone thinks
you're brilliant!

261
00:12:41,931 --> 00:12:43,322
- Really?
- Yeah!

262
00:12:43,525 --> 00:12:45,669
And in fact, everyone thinks
you're so brilliant,

263
00:12:45,863 --> 00:12:48,240
they've just made you
Employee of the Month.

264
00:12:48,394 --> 00:12:49,495
- No?
- Yes!

265
00:12:49,611 --> 00:12:50,548
- No?
- Yes!

266
00:12:50,635 --> 00:12:51,746
- No!
- Yaha!

267
00:12:52,152 --> 00:12:53,524
I don't believe it!

268
00:12:53,717 --> 00:12:56,084
- Please believe it.
- Oh my God!

269
00:12:56,963 --> 00:13:00,567
I didn't even know they had
Employee of the Month here.

270
00:13:01,252 --> 00:13:03,880
No, they brought it in just for you.

271
00:13:04,779 --> 00:13:07,001
I'm gonna go up
and thank everybody right now.

272
00:13:07,117 --> 00:13:09,083
No! Don't do that!

273
00:13:09,192 --> 00:13:10,564
What? Why not?

274
00:13:10,718 --> 00:13:12,651
They'll be embarrassed.

275
00:13:12,892 --> 00:13:15,346
It's embarrassing being thanked.

276
00:13:16,274 --> 00:13:17,897
Don't be silly.

277
00:13:22,861 --> 00:13:25,354
Events seem to be taking
a downward turn.

278
00:13:29,586 --> 00:13:31,653
I told you, stop calling me at work!

279
00:13:43,978 --> 00:13:45,891
Nice and beautiful Jen!

280
00:13:45,968 --> 00:13:47,707
Hello, Small Pall!

281
00:13:48,344 --> 00:13:50,798
Didn't... Are you...
I thought you were sick or something.

282
00:13:50,895 --> 00:13:52,421
Sick? Me? No!

283
00:13:52,537 --> 00:13:55,223
I couldn't be better, I've just won
Employee of the Month.

284
00:13:55,436 --> 00:13:58,035
So, um, you're going my way?

285
00:14:02,311 --> 00:14:04,147
Don't you dare going too fast, no!

286
00:14:12,374 --> 00:14:13,476
Just...

287
00:14:13,630 --> 00:14:15,756
hard to get your head round, you know?

288
00:14:15,872 --> 00:14:17,360
One minute, she's there and

289
00:14:17,785 --> 00:14:19,331
next minute...

290
00:14:19,898 --> 00:14:20,717
just gone.

291
00:14:20,818 --> 00:14:22,046
You know,

292
00:14:22,722 --> 00:14:24,847
I was the last person
that slept with her actually.

293
00:14:26,856 --> 00:14:29,639
She held on so tightly,
you know, it was like...

294
00:14:30,509 --> 00:14:32,383
scary, but,

295
00:14:32,750 --> 00:14:35,252
sexy scary, you know, just like...

296
00:14:44,051 --> 00:14:45,355
Jen?!

297
00:14:45,935 --> 00:14:47,191
It was her!

298
00:14:49,007 --> 00:14:51,906
What are you still doing here Bill,
for God sake, go home!

299
00:14:52,959 --> 00:14:54,659
He was the last person
to sleep with her.

300
00:14:54,717 --> 00:14:57,925
I know... Apparently, she was
pretty hard stuff!

301
00:14:59,796 --> 00:15:01,413
Bye, Small Paul!

302
00:15:01,518 --> 00:15:03,644
Bye Jen, see you.

303
00:15:06,127 --> 00:15:07,711
Oh dear, oh Lord!

304
00:15:12,735 --> 00:15:14,609
At this dreadful hour

305
00:15:14,744 --> 00:15:17,972
we're all reminded how brief
our time is here on Earth.

306
00:15:18,107 --> 00:15:19,614
Sorry, who is it who died?

307
00:15:19,749 --> 00:15:20,928
I don't know.

308
00:15:21,275 --> 00:15:24,096
I believe Mr. Renholm has a few words.

309
00:15:29,152 --> 00:15:31,741
There's one more angel in Heaven.

310
00:15:33,442 --> 00:15:35,219
God, I miss Jen!

311
00:15:35,770 --> 00:15:38,630
She reminded me of me, at her age.

312
00:15:40,669 --> 00:15:42,427
I mean, when I was her age,

313
00:15:42,543 --> 00:15:44,783
she reminded me of her age.

314
00:15:47,683 --> 00:15:50,050
She reminded me of my age at her age.

315
00:15:53,050 --> 00:15:54,692
When I was her age,

316
00:15:55,485 --> 00:15:57,803
she was reminded of me?

317
00:16:00,304 --> 00:16:02,325
I truly liked Jen.

318
00:16:03,011 --> 00:16:06,084
And that en-likening of her
became a friendship.

319
00:16:06,644 --> 00:16:08,737
A friendship which ended so suddenly,

320
00:16:09,138 --> 00:16:10,827
just twelve hours ago...

321
00:16:11,716 --> 00:16:15,909
After... what I gather, was a rather
sensational evening with Bill Crouse!

322
00:16:16,044 --> 00:16:17,763
Bill, you dog!

323
00:16:19,116 --> 00:16:20,304
Where is he?

324
00:16:20,362 --> 00:16:21,869
Still cleaning himself off.

325
00:16:23,038 --> 00:16:24,738
She was special.

326
00:16:25,975 --> 00:16:27,212
So special in fact,

327
00:16:27,289 --> 00:16:30,168
that when I called
my close personal friend

328
00:16:30,438 --> 00:16:32,989
Elton John and told him about her,

329
00:16:33,269 --> 00:16:34,766
he dropped everything,

330
00:16:34,901 --> 00:16:37,433
and offered to come here
and perform for us!

331
00:16:37,529 --> 00:16:39,539
You heard me, he's in the building!

332
00:16:39,645 --> 00:16:42,543
Elton John is going to sing
a beautiful song

333
00:16:42,644 --> 00:16:43,896
about death.

334
00:16:56,701 --> 00:16:58,576
Oh my God, this is so...

335
00:16:59,136 --> 00:17:00,353
so lovely

336
00:17:01,638 --> 00:17:03,309
Thank you, so, so much!

337
00:17:03,957 --> 00:17:06,875
Tank you, thank you, oh God!

338
00:17:15,909 --> 00:17:18,112
Could I, say a few words?

339
00:17:18,479 --> 00:17:19,600
Oh my God!

340
00:17:22,701 --> 00:17:25,416
Thank you, so, so much!

341
00:17:26,092 --> 00:17:27,261
I would...

342
00:17:27,396 --> 00:17:29,446
I would not be standing up here today

343
00:17:29,467 --> 00:17:32,854
if it wasn't for so many things
but, not least,

344
00:17:34,420 --> 00:17:36,545
- Without him...
- Hey, she isn't dead!

345
00:17:36,642 --> 00:17:37,820
Yeah!

346
00:17:40,120 --> 00:17:41,975
You lying cow!

347
00:17:43,955 --> 00:17:45,424
We want Elton!

348
00:17:45,607 --> 00:17:50,399
We want Elton! We want Elton!
We want Elton!

349
00:17:53,404 --> 00:17:54,254
Okay!

350
00:17:54,350 --> 00:17:56,772
If that Judy woman comes down,
I don't work here,

351
00:17:56,807 --> 00:17:58,653
you've never seen me,
just make up something,

352
00:17:58,654 --> 00:17:59,496
just get rid of her!

353
00:17:59,531 --> 00:18:01,811
She's easy to spot,
she's has three rows of teeth.

354
00:18:01,965 --> 00:18:05,394
Listen, if I needed to get out of
the country, very, very cheaply,

355
00:18:05,568 --> 00:18:07,346
how would I go about doing that?

356
00:18:08,892 --> 00:18:09,819
Hey you!

357
00:18:13,452 --> 00:18:14,959
What did you say about me?

358
00:18:16,031 --> 00:18:17,442
Hello there, Jen!

359
00:18:17,731 --> 00:18:19,374
I would love to stay and chat,

360
00:18:19,509 --> 00:18:21,557
but, I've just received
a telephone call,

361
00:18:21,660 --> 00:18:24,079
saying that my father
has just killed someone.

362
00:18:25,816 --> 00:18:27,378
I need to attend to.

363
00:18:27,759 --> 00:18:29,403
Did you tell everyone I was dead?

364
00:18:36,523 --> 00:18:37,486
Maybe?

365
00:18:38,084 --> 00:18:39,176
Because, um,

366
00:18:39,258 --> 00:18:40,662
they all thought I was dead.

367
00:18:47,157 --> 00:18:49,164
- I know something.
- What? What do you know?

368
00:18:49,258 --> 00:18:51,747
Bill Crouse, he's saying
he slept with you last night.

369
00:18:51,817 --> 00:18:52,815
He's saying what?

370
00:18:52,903 --> 00:18:53,755
He think you're dead,

371
00:18:53,756 --> 00:18:55,662
so he's telling everybody
you slept with him.

372
00:18:55,745 --> 00:18:57,212
He's the one you should be angry at!

373
00:18:57,312 --> 00:18:59,390
Not me! I'm insignificant!

374
00:18:59,819 --> 00:19:00,922
Bastard!

375
00:19:08,056 --> 00:19:08,866
Voicemail!

376
00:19:11,660 --> 00:19:12,271
Hey!

377
00:19:12,870 --> 00:19:13,785
It's Jen!

378
00:19:13,996 --> 00:19:16,122
Stop telling everyone I slept with you!

379
00:19:16,491 --> 00:19:17,748
You bastard!

380
00:19:20,148 --> 00:19:21,393
Must have gone home.

381
00:19:22,343 --> 00:19:24,351
Oh God, my throat hurts.

382
00:19:25,791 --> 00:19:28,709
Well, it's probably all that shouting.

383
00:19:30,467 --> 00:19:32,149
I'm talking, aren't I?

384
00:19:33,482 --> 00:19:35,588
Can you get me his address?

385
00:19:36,129 --> 00:19:38,553
Yes, it might be a bit difficult.

386
00:19:38,602 --> 00:19:40,822
I have to hack into
his private company account,

387
00:19:40,823 --> 00:19:41,955
it might take some time.

388
00:19:43,104 --> 00:19:44,032
There we go.

389
00:19:47,343 --> 00:19:49,391
Oh, I really don't feel very well.

390
00:19:49,604 --> 00:19:51,633
Well, you look terrible.

391
00:19:53,507 --> 00:19:56,406
I'm still talking, aren't I?

392
00:20:05,446 --> 00:20:07,533
<i>You have one new message.</i>

393
00:20:07,726 --> 00:20:08,770
<i>Hey!</i>

394
00:20:09,001 --> 00:20:10,103
<i>It's Jen!</i>

395
00:20:10,335 --> 00:20:12,963
<i>Stop telling people I slept with you!</i>

396
00:20:13,137 --> 00:20:14,856
<i>You bastard!</i>

397
00:20:25,791 --> 00:20:27,511
Where Roy?

398
00:20:29,308 --> 00:20:30,583
Roy is dead!

399
00:20:44,674 --> 00:20:47,022
No, no! Don't breakdown!

400
00:20:47,138 --> 00:20:49,051
Don't break down, you bitch!

401
00:20:52,399 --> 00:20:55,665
Oh my God, oh my God!

402
00:20:58,602 --> 00:21:00,032
Bloody hell!

403
00:21:03,462 --> 00:21:06,744
Why you do that to me
when I need this chance?

404
00:21:06,745 --> 00:21:09,075
Oh for God's sake!
Now my car is bloody open

405
00:21:09,076 --> 00:21:10,732
and I don't know what it means!

406
00:21:13,624 --> 00:21:18,706
Come on, come on Ritchley...
Street, Ritchley Street.

407
00:21:21,266 --> 00:21:24,841
My God, okay, I'm... Right.

408
00:21:28,393 --> 00:21:30,248
Probably guys, probably guys,

409
00:21:30,325 --> 00:21:32,335
leaving message, funny message,

410
00:21:32,470 --> 00:21:36,470
probably, Bob,
it's Bob leaving funny messages.

411
00:21:36,586 --> 00:21:39,117
And the, the floating head.

412
00:21:42,634 --> 00:21:43,851
It's all stress,

413
00:21:44,063 --> 00:21:47,636
a bit stress is all and...
just fun and games.

414
00:21:54,503 --> 00:21:55,991
Oh God!

415
00:22:01,772 --> 00:22:03,550
Oh God!

416
00:22:03,812 --> 00:22:07,051
Let me in! Let me in Bill!

417
00:22:07,115 --> 00:22:07,946
No!

418
00:22:08,120 --> 00:22:11,270
It's raining! Please, let me in!

419
00:22:11,366 --> 00:22:13,125
No, you're never coming in!

420
00:22:13,240 --> 00:22:16,641
Stop telling people I slept with you!

421
00:22:16,727 --> 00:22:18,698
I will! I will! I'm sorry!

422
00:22:18,862 --> 00:22:20,176
Bastard!

423
00:22:20,939 --> 00:22:23,412
You bastard!

424
00:22:24,082 --> 00:22:25,789
Oh no! I'm sorry!

425
00:22:25,925 --> 00:22:28,446
You bastard!

426
00:22:44,769 --> 00:22:49,131
<i>Small Paul, you're gone now.</i>

427
00:22:49,211 --> 00:22:53,233
<i>You pushed that trolley
a little too hard,</i>

428
00:22:53,338 --> 00:22:58,564
<i>You felt a twinge,
it was your heart and you're dead now...</i>

429
00:23:01,072 --> 00:23:05,468
<i>And now you're running four feet
in heaven...</i>

430
00:23:06,107 --> 00:23:08,426
- Did you know him? - No.

431
00:23:13,319 --> 00:23:15,425
I don't think that's Elton John.