1
00:00:30,644 --> 00:00:32,924
Transcript: Travis

2
00:00:33,117 --> 00:00:35,629
Adaptation: Any1, Sixe

3
00:00:38,326 --> 00:00:39,484
Here we are!

4
00:00:40,556 --> 00:00:42,717
Your place... your place...

5
00:00:43,958 --> 00:00:46,659
La maison... de la femme...

6
00:00:47,499 --> 00:00:49,789
El casa del signorita...

7
00:00:53,275 --> 00:00:54,660
Das haus die Frau...

8
00:00:58,014 --> 00:00:59,014
Good night!

9
00:00:59,521 --> 00:01:02,061
I'll just walk you to the door.

10
00:01:02,132 --> 00:01:04,128
- There's no need.
- That's okay.

11
00:01:10,141 --> 00:01:11,146
So...

12
00:01:12,182 --> 00:01:13,987
I guess this is "arrivederci".

13
00:01:15,756 --> 00:01:17,755
I'd quite fancy a cup of tea though.

14
00:01:20,106 --> 00:01:21,383
A glass of water maybe.

15
00:01:23,522 --> 00:01:24,966
Perhaps a little lie down.

16
00:01:25,029 --> 00:01:26,039
Look...

17
00:01:26,586 --> 00:01:28,207
I have to tell you something.

18
00:01:28,383 --> 00:01:29,200
What?

19
00:01:29,201 --> 00:01:31,535
I wanted to tell you
in the restaurant but...

20
00:01:31,706 --> 00:01:34,030
it... a good time
never came up and I...

21
00:01:34,146 --> 00:01:36,443
- I wasn't gonna say it but...
- Say it.

22
00:01:37,285 --> 00:01:38,285
I have to.

23
00:01:41,547 --> 00:01:42,547
What?

24
00:01:42,899 --> 00:01:44,510
You've got shit on your head.

25
00:01:48,409 --> 00:01:49,409
What?

26
00:01:50,611 --> 00:01:52,333
There's some shit on your head.

27
00:01:52,414 --> 00:01:53,414
What?

28
00:01:53,478 --> 00:01:55,204
Where? Where?

29
00:01:55,836 --> 00:01:57,254
It's been there forever.

30
00:01:57,342 --> 00:01:59,573
Oh God! Oh God!

31
00:02:02,439 --> 00:02:03,439
It's chocolate!

32
00:02:03,695 --> 00:02:04,499
It's chocolate!

33
00:02:04,500 --> 00:02:05,500
It's not shit!

34
00:02:05,545 --> 00:02:06,398
It's not shit!

35
00:02:06,399 --> 00:02:07,399
It's..

36
00:02:07,897 --> 00:02:08,897
It's my dessert.

37
00:02:09,852 --> 00:02:11,810
I don't know
how it got there but uh...

38
00:02:11,811 --> 00:02:12,866
It's on my fingers.

39
00:02:13,560 --> 00:02:14,560
It's not shit!

40
00:02:15,719 --> 00:02:16,774
Thank God for that!

41
00:02:18,976 --> 00:02:21,571
You know, I just spent the last hour

42
00:02:21,892 --> 00:02:24,693
thinking you were some sort
of disgusting tramp.

43
00:02:24,774 --> 00:02:26,076
Oh no!

44
00:02:27,827 --> 00:02:28,827
That's funny!

45
00:02:30,995 --> 00:02:32,921
- Oh God look!
- What?

46
00:02:33,021 --> 00:02:35,439
It's chocolate!

47
00:02:38,339 --> 00:02:40,005
How about that cup of tea now?

48
00:02:43,693 --> 00:02:46,294
I was a perfect gentleman last night
from beginning to end

49
00:02:46,295 --> 00:02:49,389
but she can't see past
a little bit of shit on my forehead!

50
00:02:49,412 --> 00:02:50,900
I thought you said it was chocolate.

51
00:02:50,935 --> 00:02:52,981
Yes, chocolate, yes!
I meant chocolate.

52
00:02:54,319 --> 00:02:56,041
I forgot to turn this thing on.

53
00:02:57,263 --> 00:02:58,263
<i>Hello, IT. .</i>

54
00:02:59,160 --> 00:03:01,548
<i>Hi, uh, something's wrong
with my computer.</i>

55
00:03:01,719 --> 00:03:04,015
<i>Have you tried
turning it off and on again?<i>

56
00:03:04,016 --> 00:03:05,629
<i>Oh No! No, oh Dear, thanks.</i>

57
00:03:06,163 --> 00:03:07,278
<i>You're welcome then.</i>

58
00:03:08,487 --> 00:03:09,532
It's typical.

59
00:03:09,579 --> 00:03:12,529
The one time I manage
to bamboozle a girl in the building

60
00:03:12,530 --> 00:03:14,752
into going out with me
and this happens.

61
00:03:15,374 --> 00:03:16,725
She's gonna tell everyone!

62
00:03:16,842 --> 00:03:18,999
Oh come on! She's a receptionist,

63
00:03:19,161 --> 00:03:20,564
she'd hardly gossip.

64
00:03:21,841 --> 00:03:23,557
Besides it's a boring story.

65
00:03:23,846 --> 00:03:25,631
Yeah... that's boring.

66
00:03:25,949 --> 00:03:27,893
It's boring, she won't tell anyone.

67
00:03:39,744 --> 00:03:40,744
Hello.

68
00:03:48,012 --> 00:03:49,012
Sorry guys.

69
00:03:55,219 --> 00:03:56,782
She might have told Jen.

70
00:04:00,127 --> 00:04:01,842
No, that's what I'm saying.

71
00:04:02,126 --> 00:04:03,092
It wasn't. It was...

72
00:04:03,435 --> 00:04:05,379
It was chocolate. It was chocolate.

73
00:04:06,286 --> 00:04:07,939
I don't know...
I don't know... it...

74
00:04:07,940 --> 00:04:09,944
it got on my fingers
and then I must have...

75
00:04:09,945 --> 00:04:11,101
Listen...

76
00:04:11,181 --> 00:04:13,018
how did you even hear
about this, mom?

77
00:04:15,250 --> 00:04:18,244
Moss... Moss,
there's a spider in my office.

78
00:04:18,308 --> 00:04:20,752
Could you deal with it?
I just hate spiders.

79
00:04:24,160 --> 00:04:27,722
I'm not overly fond of them
myself there, Jen.

80
00:04:28,558 --> 00:04:29,673
How...

81
00:04:31,845 --> 00:04:35,215
How big is... Whoa! Hey now!

82
00:04:35,353 --> 00:04:36,740
I'll tell you what.

83
00:04:36,775 --> 00:04:39,750
I actually recommend
my good friend Roy there

84
00:04:39,797 --> 00:04:42,028
as the go-to guy on anything

85
00:04:42,492 --> 00:04:44,136
with more than 7 eyes.

86
00:04:45,013 --> 00:04:47,374
He's on a very brief
personal call right now.

87
00:04:47,409 --> 00:04:48,548
Come, Moss. Don't be silly.

88
00:04:48,583 --> 00:04:50,734
You're right, I'm being silly.
Wrap it up, Roy!

89
00:04:51,568 --> 00:04:53,294
It's just a spider. It's fine.

90
00:04:54,392 --> 00:04:57,451
Oh look! It seems to have left
of its own volition.

91
00:04:57,552 --> 00:04:58,844
Oh, be a man, Moss!

92
00:04:58,898 --> 00:05:01,378
You're right, I'll be a man.
I'm a man.

93
00:05:02,968 --> 00:05:04,468
Please don't shut the door.

94
00:05:05,979 --> 00:05:06,979
I  won't.

95
00:05:07,424 --> 00:05:08,424
Goodbye, Jen.

96
00:05:13,031 --> 00:05:14,462
That was my mother.

97
00:05:14,736 --> 00:05:16,291
How did she hear about this?

98
00:05:16,340 --> 00:05:18,673
Oh, was that the woman
who phoned earlier?

99
00:05:18,739 --> 00:05:20,793
'Cause I've just been
telling everybody.

100
00:05:20,794 --> 00:05:23,697
Why would you want to go out
with Patricia anyway?

101
00:05:23,749 --> 00:05:26,717
I mean, her last boyfriend
was Simon from 3rd.

102
00:05:27,104 --> 00:05:28,637
- Jen!
- Hey, you!

103
00:05:29,163 --> 00:05:30,163
Hello there.

104
00:05:30,547 --> 00:05:31,984
Have you caught it yet?

105
00:05:32,313 --> 00:05:33,623
Not quite, Jen.

106
00:05:34,264 --> 00:05:36,430
The spider's managed
to walk behind me.

107
00:05:37,170 --> 00:05:39,003
And it's between me and the door.

108
00:05:40,628 --> 00:05:42,059
So I can't get out.

109
00:05:43,532 --> 00:05:45,754
Simon from 3rd
is the one with the hair.

110
00:05:46,429 --> 00:05:48,304
Oh, him! Oh, he's awful.

111
00:05:48,790 --> 00:05:51,563
He once ate
a copy of Loaded for a bet.

112
00:05:53,438 --> 00:05:55,433
- She liked him?
- Apparently.

113
00:05:55,691 --> 00:05:57,230
That explains everything!

114
00:05:57,304 --> 00:06:00,470
She likes the kind of big obnoxious guy
who eats Loaders.

115
00:06:03,555 --> 00:06:04,555
Hello, Daniel.

116
00:06:05,206 --> 00:06:06,011
How  are you?

117
00:06:06,012 --> 00:06:07,466
I'm great, thanks. How are you?

118
00:06:07,467 --> 00:06:08,467
I'm great.

119
00:06:08,717 --> 00:06:11,384
Well, can either of you tell me...
Okay... Okay...

120
00:06:11,804 --> 00:06:16,184
who wrote Missa Brevis in D minor?
Was it Mozart or Beethoven?

121
00:06:17,345 --> 00:06:20,428
That was a certain Mr. Mozart,
I think you'll find.

122
00:06:20,598 --> 00:06:23,137
Oh right, do you know a lot
about classical music?

123
00:06:23,219 --> 00:06:25,497
- Petit pois.
- Oh, right.

124
00:06:27,384 --> 00:06:29,298
Mozart, Beethoven, 50/50 ain't it?

125
00:06:30,991 --> 00:06:33,720
Actually, no, you were right.
It was Mozart.

126
00:06:33,791 --> 00:06:34,848
Well it's... It was...

127
00:06:34,866 --> 00:06:36,932
Okay, so, let's try another one.

128
00:06:37,387 --> 00:06:40,829
Okay. Here we go.
In what year did Mozart die?

129
00:06:40,913 --> 00:06:43,754
- Was it 1780 or 1791?
- 1791.

130
00:06:44,593 --> 00:06:48,233
Yes! Yes! Bang on! Good!

131
00:06:49,287 --> 00:06:51,342
God!
I hope he doesn't come in again!

132
00:06:52,375 --> 00:06:53,650
- Jen!
- Yes!

133
00:06:54,145 --> 00:06:57,589
Is Roy off the phone yet?
The spider is now upon my person.

134
00:07:07,330 --> 00:07:10,054
You guys don't mind me
chilling out in here, do you?

135
00:07:10,136 --> 00:07:11,886
No. Chill out!

136
00:07:13,212 --> 00:07:15,386
So did you see the match last night?

137
00:07:15,810 --> 00:07:17,544
- Oh yeah!
- Oh yeah!

138
00:07:17,629 --> 00:07:18,712
- God!
- Did we?

139
00:07:19,312 --> 00:07:20,580
What a match!

140
00:07:22,388 --> 00:07:24,795
All of the players running up...

141
00:07:25,276 --> 00:07:26,023
the pitch...

142
00:07:26,024 --> 00:07:29,321
and running down it again.
Such athleticism!

143
00:07:30,257 --> 00:07:31,842
What was Rooney thinking over there?

144
00:07:32,821 --> 00:07:35,476
Who knows! That's Rooney!

145
00:07:35,602 --> 00:07:38,647
He is a foolish boy!

146
00:07:40,446 --> 00:07:43,755
Yeah but... that referee...
I'm sorry the referee is just an idiot.

147
00:07:43,816 --> 00:07:47,327
Yes, he was certainly
the villain of the piece.

148
00:07:48,954 --> 00:07:50,386
Daniel, you're still here.

149
00:07:50,764 --> 00:07:52,810
Yeah, just...
just having a little break.

150
00:07:52,811 --> 00:07:54,477
Do you wanna come and join me?

151
00:07:55,768 --> 00:07:56,655
Stop it!

152
00:07:58,381 --> 00:08:01,423
Well, I'd better...
I'd better... hit this way.

153
00:08:01,624 --> 00:08:04,323
Oh by the way, guys, if you need
any help with any more spiders,

154
00:08:04,324 --> 00:08:06,036
just give me a call, okay?

155
00:08:06,177 --> 00:08:08,244
Yeah, we'll be fine, I think, thanks.

156
00:08:08,322 --> 00:08:10,013
Actually, do you have a card?

157
00:08:11,054 --> 00:08:13,097
That is good. See you.

158
00:08:17,872 --> 00:08:18,872
You like him!

159
00:08:19,975 --> 00:08:20,934
Who?

160
00:08:21,580 --> 00:08:24,315
Oh, that good looking man
with "Security"

161
00:08:24,362 --> 00:08:25,596
written on his shoulders? Yes!

162
00:08:25,784 --> 00:08:26,784
Yes, I do.

163
00:08:27,440 --> 00:08:28,606
He seems really nice.

164
00:08:29,310 --> 00:08:30,225
Nice!

165
00:08:30,296 --> 00:08:33,361
'Cause that's what you women want!
Nice guys!

166
00:08:34,095 --> 00:08:34,997
<i>Hello, IT! .</i>

167
00:08:34,998 --> 00:08:36,775
What are you talking about, Roy?

168
00:08:36,878 --> 00:08:40,107
If my date last night proved anything,
it's that you, ladies,

169
00:08:40,142 --> 00:08:41,759
you don't want nice guys.

170
00:08:41,794 --> 00:08:43,565
I think if your date proved anything,
Roy,

171
00:08:43,566 --> 00:08:46,732
it's that women like men
without poo on their foreheads.

172
00:08:47,266 --> 00:08:48,482
That's our type.

173
00:08:49,664 --> 00:08:51,261
That  has nothing to do with this.

174
00:08:51,308 --> 00:08:54,208
She clearly just didn't want
a gentleman in her life.

175
00:08:54,275 --> 00:08:56,429
I hate to generalize, you know that.

176
00:08:56,678 --> 00:08:59,144
But all women just want bastards.

177
00:09:00,485 --> 00:09:04,112
That women liking bastards thing
is a total myth.

178
00:09:04,179 --> 00:09:07,772
<i>I've tried turning it off and on again
and nothing happened.</i>

179
00:09:07,841 --> 00:09:09,396
<i>Is it definitely plugged in?</i>

180
00:09:10,651 --> 00:09:11,651
<i>Oh, let me have a look.</i>

181
00:09:12,200 --> 00:09:13,955
<i>Oh, sorry, that's it, thanks very much.</i>

182
00:09:13,956 --> 00:09:15,011
<i>You're welcome then.</i>

183
00:09:15,372 --> 00:09:17,285
It's just not true.

184
00:09:17,507 --> 00:09:21,141
Women don't want gentlemen,
they want bullies and thugs.

185
00:09:21,460 --> 00:09:22,510
I'll prove it to you.

186
00:09:22,511 --> 00:09:24,344
How will you prove it to me, Roy?

187
00:09:25,289 --> 00:09:26,417
Oh! Okay.

188
00:09:26,558 --> 00:09:30,313
I'll put a...
I'll put on a classified ad online.

189
00:09:30,622 --> 00:09:31,471
A classified?

190
00:09:31,472 --> 00:09:33,070
Yeah, like a lonely hearts thing

191
00:09:33,071 --> 00:09:34,577
but I'll make it sound psychotic.

192
00:09:34,578 --> 00:09:36,880
And I still bet
I'll get a bezillion replies.

193
00:09:36,881 --> 00:09:37,828
Oh, dear God.

194
00:09:37,829 --> 00:09:38,829
Come on. How much?

195
00:09:38,880 --> 00:09:44,016
Okay. Yeah, I'll give you
20 quid if you get 1 response.

196
00:09:44,087 --> 00:09:44,944
You're on!

197
00:09:45,061 --> 00:09:46,068
Oh my God, no!

198
00:09:47,271 --> 00:09:50,145
It's all about body fluids
with you, isn't it?

199
00:09:52,829 --> 00:09:54,581
Come on, Moss! We need to get to work.

200
00:09:54,582 --> 00:09:55,534
Work?

201
00:09:55,535 --> 00:09:58,757
Yeah, we need to post
a classified lonely hearts ad online

202
00:09:58,786 --> 00:10:01,250
that makes me sound like a psycho
so that I can prove to Jen

203
00:10:01,285 --> 00:10:04,359
that all women love bastards
and therefore win 20 pounds.

204
00:10:04,801 --> 00:10:07,334
Thank God!
I thought that thing was broken.

205
00:10:07,902 --> 00:10:10,808
"Shut up! Do what I tell you!
I'm not interested!"

206
00:10:12,356 --> 00:10:16,467
These are just some of the things
you'll be hearing if you answer this ad.

207
00:10:17,576 --> 00:10:20,576
"I'm an idiot and
I don't care about anyone but myself."

208
00:10:21,531 --> 00:10:23,252
"PS: No dogs."

209
00:10:25,383 --> 00:10:27,049
- That's good.
- What's yours?

210
00:10:27,826 --> 00:10:29,317
Mine doesn't look any good now.

211
00:10:29,435 --> 00:10:30,438
Go on.

212
00:10:31,296 --> 00:10:32,972
"I'm going to murder you."

213
00:10:35,699 --> 00:10:37,326
"You bloody woman!"

214
00:10:40,164 --> 00:10:42,413
Might want to play a bit hard to get.

215
00:10:44,162 --> 00:10:46,487
If you were a murderer,
what would your nickname be?

216
00:10:46,505 --> 00:10:47,797
Mine would be "The Gardener",

217
00:10:47,844 --> 00:10:51,002
'cause I'd always leave a rose
at the scene of the crime.

218
00:10:51,525 --> 00:10:53,510
What would your murder weapon be?

219
00:10:54,480 --> 00:10:55,480
A hammer.

220
00:10:57,783 --> 00:10:58,783
Hey, Moss.

221
00:11:01,836 --> 00:11:04,058
"Would you like to go
for dinner with me?"

222
00:11:05,428 --> 00:11:08,094
"And then maybe back to my place?"

223
00:11:09,249 --> 00:11:12,461
I'd love to, Jen, but I'm actually
helping Roy at the moment.

224
00:11:12,462 --> 00:11:15,799
No. No, no, no.
Who does this remind you of, yeah?

225
00:11:16,568 --> 00:11:18,456
"Do you wanna go on a date with me?"

226
00:11:20,994 --> 00:11:22,039
Gandhi?

227
00:11:24,080 --> 00:11:26,172
No,  no, the other one. Bono.

228
00:11:27,711 --> 00:11:29,379
No, no, I was doing Roy.

229
00:11:29,487 --> 00:11:31,740
Yes, yes, it's like there was 2 of me.

230
00:11:32,597 --> 00:11:34,874
Come on.
Get your money ready. 20 pounds.

231
00:11:35,951 --> 00:11:37,689
Hey! Hello.

232
00:11:40,251 --> 00:11:42,142
It's security.

233
00:11:42,458 --> 00:11:44,771
I hope I'm not in any danger.

234
00:11:44,974 --> 00:11:45,974
Oh my God!

235
00:11:46,372 --> 00:11:47,372
It's  not shit!

236
00:11:49,998 --> 00:11:51,577
On my head, it's not shit!

237
00:11:52,006 --> 00:11:53,357
It's chocolate spread.

238
00:11:53,681 --> 00:11:54,797
- Taste it!
- No!

239
00:11:54,832 --> 00:11:55,771
Look!

240
00:11:55,853 --> 00:11:56,887
Look!

241
00:12:00,598 --> 00:12:01,803
Ringo Starr?

242
00:12:03,403 --> 00:12:05,672
I'll be wondering about that all night!

243
00:12:05,855 --> 00:12:07,257
How do you know about this site?

244
00:12:07,258 --> 00:12:08,258
I'm a member.

245
00:12:09,818 --> 00:12:12,864
Really?
You do the whole lonely hearts thing?

246
00:12:13,628 --> 00:12:16,797
I'm a 32-year-old IT man
who works in a basement.

247
00:12:17,581 --> 00:12:19,803
Yes, I do the whole lonely hearts thing.

248
00:12:22,942 --> 00:12:26,155
So, as you can clearly see, it wasn't...

249
00:12:27,795 --> 00:12:29,828
excrement, it was chocolate.

250
00:12:30,006 --> 00:12:32,060
I was playing a joke. It was chocolate.

251
00:12:32,061 --> 00:12:34,561
Jen, it's okay, I've got it.
Don't worry about it.

252
00:12:34,808 --> 00:12:36,752
Listen, what are you doing tonight?

253
00:12:38,366 --> 00:12:40,588
- I've got a response!
- That was quick!

254
00:12:40,617 --> 00:12:43,138
Yeah,
why is it taking so long to download?

255
00:12:43,197 --> 00:12:45,164
Something happened
with the router this morning.

256
00:12:45,199 --> 00:12:47,012
We're back to pre-broadband speed.

257
00:12:48,077 --> 00:12:49,077
Dear...

258
00:12:55,989 --> 00:12:57,532
But we're on the phone now.

259
00:12:57,592 --> 00:12:59,195
Why can't you just ask me now?

260
00:12:59,270 --> 00:13:00,866
Can't Jen, trust me.

261
00:13:01,447 --> 00:13:02,645
Why should I trust you?

262
00:13:02,646 --> 00:13:04,590
Because I know what's good for you.

263
00:13:06,594 --> 00:13:10,234
Listen, you have to be home between
8 and 9 tonight, are you gonna be home?

264
00:13:10,281 --> 00:13:12,195
Okay, this sounds exciting.

265
00:13:12,221 --> 00:13:14,957
It might not happen, okay?
But hopefully it will.

266
00:13:14,979 --> 00:13:16,942
Now I don't know
if this is exciting or not...

267
00:13:16,988 --> 00:13:19,145
Jen... Jen, it's exciting!

268
00:13:23,253 --> 00:13:26,254
"...would be very interested in
meeting up and talking it over."

269
00:13:26,255 --> 00:13:27,255
"Love, Rebecca."

270
00:13:27,403 --> 00:13:29,347
Oh my God! What did we write again?

271
00:13:29,457 --> 00:13:30,901
It started with "shut up".

272
00:13:32,457 --> 00:13:33,662
There's more.

273
00:13:34,117 --> 00:13:36,617
"I attached a photograph
for your approval...

274
00:13:36,618 --> 00:13:38,391
"or not!" Exclamation mark.

275
00:13:38,554 --> 00:13:41,387
All right, let's have a look at you,
you mad thing.

276
00:13:45,168 --> 00:13:46,419
Nice hair so far.

277
00:13:47,569 --> 00:13:50,514
- Eyebrows seem normal.
- Two eyes.

278
00:13:51,279 --> 00:13:53,021
That's the best amount of eyes.

279
00:13:54,285 --> 00:13:55,431
Nice eyes too.

280
00:13:56,234 --> 00:13:58,842
It has to go around.
She must have a shite nose.

281
00:14:01,576 --> 00:14:03,540
Do you remember the Internet
at this speed?

282
00:14:03,609 --> 00:14:05,581
Up all night and you'd see 8 women.

283
00:14:08,559 --> 00:14:10,503
- Good nose.
- That is a good nose.

284
00:14:10,614 --> 00:14:14,780
The mouth has to be wrong. There's gotta
be something wrong with her mouth.

285
00:14:14,825 --> 00:14:16,922
Now that's a good looking woman.

286
00:14:17,774 --> 00:14:19,607
Should I tell Jen we got a reply?

287
00:14:20,976 --> 00:14:21,976
Roy?

288
00:14:23,034 --> 00:14:24,034
Roy?

289
00:14:25,739 --> 00:14:26,739
Roy!

290
00:14:28,392 --> 00:14:29,392
Roy!

291
00:14:36,900 --> 00:14:39,008
Hello, Daniel! I mean, hello.

292
00:14:39,903 --> 00:14:43,846
Jen? Hello, it's Chris Tarrant from
"Who Wants to be a Millionaire?"

293
00:14:43,909 --> 00:14:46,422
Oh my God! Oh my God!

294
00:14:46,469 --> 00:14:47,822
Chris Tarrant, hello!

295
00:14:48,324 --> 00:14:51,048
Oh my God! Oh my God! Oh my God!

296
00:14:51,158 --> 00:14:53,378
Jen the next voice you hear
is gonna be Daniel's...

297
00:14:53,413 --> 00:14:55,827
- Daniel's, Ok.
- But before I put you on with him,

298
00:14:55,828 --> 00:14:57,828
he's asked me to ask you a question.

299
00:14:57,930 --> 00:14:59,263
Okay, you can ask me a question.

300
00:14:59,331 --> 00:15:03,598
Yeah, he wants to know if you'll go out
for dinner with him tomorrow night.

301
00:15:04,311 --> 00:15:06,549
Oh really? Yes!

302
00:15:06,619 --> 00:15:09,143
Yes, of course, I will!
Of course! Oh yes! Yes!

303
00:15:09,213 --> 00:15:10,213
Yes, of course!

304
00:15:11,576 --> 00:15:13,096
Yes, that's great, Jen.

305
00:15:13,665 --> 00:15:15,475
Now let me fill you in
on what's happening.

306
00:15:15,476 --> 00:15:18,864
Daniel's doing very well although
he has just used his 50/50.

307
00:15:18,916 --> 00:15:21,382
Now, here's Daniel.
Daniel, you got 30 seconds.

308
00:15:21,476 --> 00:15:23,216
Starting from... now!

309
00:15:23,382 --> 00:15:24,787
- Okay, Jen.
- Hi, Daniel.

310
00:15:24,788 --> 00:15:26,689
<i>Yeah, hi Jen, Jen, uh, okay...</i>

311
00:15:26,942 --> 00:15:28,664
<i>who composed The Wooden Prince?</i>

312
00:15:28,792 --> 00:15:31,159
<i>Was it A) Bartok, B) Chopin ?</i>

313
00:15:37,154 --> 00:15:39,580
Did you see
"I'm a Millionaire" last night?

314
00:15:39,604 --> 00:15:42,035
"Who Wants to be a Millionaire?"
it's called. No, I didn't.

315
00:15:42,070 --> 00:15:44,582
Well, you are not going to
Adam and beliEVE this.

316
00:15:45,408 --> 00:15:47,925
Okay, do these make me
look like a bastard?

317
00:15:48,618 --> 00:15:50,149
Why do you wanna look like a bastard?

318
00:15:50,150 --> 00:15:52,602
I've got that date tonight,
she's expecting a monster.

319
00:15:52,603 --> 00:15:54,607
Do these me make look like a bastard?

320
00:15:54,608 --> 00:15:56,135
They make you look like an idiot.

321
00:15:56,170 --> 00:15:57,866
- Anyway, did you see it?
- What?

322
00:15:58,125 --> 00:16:00,239
"Do you want to be a Millionaire?"

323
00:16:00,566 --> 00:16:02,945
"Who Wants to be a Millionaire?"

324
00:16:04,790 --> 00:16:07,432
"Who wants to be a person who is wrong?"

325
00:16:08,569 --> 00:16:09,790
What's happened?

326
00:16:10,027 --> 00:16:11,193
- If I may...
- Sure.

327
00:16:11,531 --> 00:16:13,253
Guess who was on it last night!

328
00:16:13,882 --> 00:16:15,823
- You!
- No, Daniel.

329
00:16:16,000 --> 00:16:18,300
- King of men.
- Daniel was on "Millionaire"?

330
00:16:18,301 --> 00:16:19,356
Indeed he was, sir.

331
00:16:19,515 --> 00:16:21,753
And it was the
"Do you want to phone a friend"...

332
00:16:21,800 --> 00:16:23,737
- It's just "Phone a friend".
- Anyway.

333
00:16:24,603 --> 00:16:25,777
She was the "phone a friend"?

334
00:16:25,836 --> 00:16:28,571
- She was the "phone a friend".
- You're the "phone a friend"!

335
00:16:28,572 --> 00:16:29,568
What was the subject?

336
00:16:29,569 --> 00:16:32,957
Classical music is the extraordinary
answer to that question.

337
00:16:33,250 --> 00:16:37,231
So who of course is Daniel, our estimed
temp security guard, going to phone

338
00:16:37,232 --> 00:16:38,954
but Jonathan Miller over here.

339
00:16:39,343 --> 00:16:43,394
Think Daniel was on Millionaire.
Too bad it wasn't 50/50.

340
00:16:44,289 --> 00:16:45,289
It was.

341
00:16:46,134 --> 00:16:47,546
- And?
- I got it wrong.

342
00:16:48,495 --> 00:16:50,064
I cost 31,000 pounds.

343
00:16:51,154 --> 00:16:52,680
It isn't funny though.

344
00:16:52,749 --> 00:16:54,405
Now I have to go on a date with him.

345
00:16:54,406 --> 00:16:56,402
Wow! The rules on that show
have really changed!

346
00:16:56,437 --> 00:16:58,775
No, you don't understand.

347
00:16:59,109 --> 00:17:00,267
He asked me out

348
00:17:00,366 --> 00:17:01,366
on the show

349
00:17:01,540 --> 00:17:03,327
before I got the question wrong.

350
00:17:03,397 --> 00:17:05,963
It was all very romantic.

351
00:17:06,329 --> 00:17:07,329
But now...

352
00:17:07,725 --> 00:17:09,638
I don't think he likes me now.

353
00:17:10,176 --> 00:17:12,932
And I don't wanna go out with
the biggest loser in England...

354
00:17:12,984 --> 00:17:14,398
There you are!

355
00:17:14,583 --> 00:17:15,583
Daniel...

356
00:17:16,434 --> 00:17:17,434
- Hello.
- Hi.

357
00:17:19,592 --> 00:17:21,842
- Gonna go for this meal then.
- Yeah.

358
00:17:22,349 --> 00:17:23,435
Yeah, I can't wait.

359
00:17:23,482 --> 00:17:26,242
I'm gonna take you to one
of my favourite restaurants.

360
00:17:26,301 --> 00:17:27,311
Right.

361
00:17:31,002 --> 00:17:32,002
I'll pay.

362
00:17:32,823 --> 00:17:35,182
- It really is the least I can do.
- Oh yeah...

363
00:17:40,388 --> 00:17:41,388
Oh my God!

364
00:17:41,691 --> 00:17:43,206
Did you see that look?

365
00:17:43,532 --> 00:17:46,585
I  have a dinner date with that man.

366
00:17:46,654 --> 00:17:49,660
If I'm not here tomorrow,
I want you to call the police.

367
00:17:49,801 --> 00:17:52,833
Maybe not the best time to mention it,
but also you owe me 20 quids.

368
00:17:52,834 --> 00:17:54,996
What? You got a response?

369
00:17:55,394 --> 00:17:57,282
With the one that ended: "No dogs"?

370
00:17:57,545 --> 00:17:58,545
Yep.

371
00:17:58,547 --> 00:18:00,211
Oh my God, brilliant! More money gone!

372
00:18:00,247 --> 00:18:02,700
Now the money that Daniel lost
wasn't really yours, was it?

373
00:18:02,701 --> 00:18:05,145
It would have been
if I'd married him, Moss.

374
00:18:06,558 --> 00:18:07,859
I need a good restaurant.

375
00:18:07,860 --> 00:18:11,526
Maybe if he likes the food,
he won't beat me to death with a shoe.

376
00:18:12,012 --> 00:18:15,205
I thought you said you'd take him
to one of your favourite restaurants.

377
00:18:15,252 --> 00:18:16,707
Haven't you got it yet?

378
00:18:17,088 --> 00:18:18,234
That's what I do:

379
00:18:18,433 --> 00:18:19,433
I lie...

380
00:18:19,714 --> 00:18:21,851
I lie and I lie and...

381
00:18:21,940 --> 00:18:22,940
I lie!

382
00:18:23,848 --> 00:18:26,008
Well, I went to a good place recently.

383
00:18:26,096 --> 00:18:27,939
Nice atmosphere. Food's great.

384
00:18:28,151 --> 00:18:30,049
- Really?
- Yes. Why?

385
00:18:30,552 --> 00:18:33,005
Sorry, I just didn't have you down
as a restaurant person.

386
00:18:33,006 --> 00:18:34,161
I'm a restaurant person.

387
00:18:34,162 --> 00:18:35,897
Why wouldn't I be a restaurant person?

388
00:18:35,932 --> 00:18:37,060
Cheese string?

389
00:18:38,044 --> 00:18:39,213
No, thanks.

390
00:18:39,279 --> 00:18:40,425
What's this place called?

391
00:18:40,426 --> 00:18:41,426
Messijoes.

392
00:18:41,676 --> 00:18:44,064
- Is that French?
- French? No, it's English.

393
00:18:45,182 --> 00:18:47,488
Okay, thank you, Moss. Yeah, I'll uh...

394
00:18:47,489 --> 00:18:48,600
I'll give that a go.

395
00:18:49,037 --> 00:18:51,459
Okay, I need a place to bring this girl

396
00:18:51,743 --> 00:18:54,215
that says: "I'm a bastard."

397
00:18:54,400 --> 00:18:56,511
Well, I went to a good place recently.

398
00:18:56,752 --> 00:18:59,651
Nice atmosphere. Food's great.
It's got a bit of an edge.

399
00:18:59,652 --> 00:19:00,757
Yeah? What's it called?

400
00:19:00,758 --> 00:19:01,758
Messijoes.

401
00:19:02,208 --> 00:19:03,257
What is that? Spanish?

402
00:19:03,339 --> 00:19:07,038
No! Spanish? It's English. Messijoes.

403
00:19:26,352 --> 00:19:29,745
Okey dokey,
you get the firecracker nachos.

404
00:19:30,163 --> 00:19:33,611
And you are getting
the prawn bites with spicy salsa.

405
00:19:37,970 --> 00:19:41,831
This seems an unusual place
for someone like you to want to meet.

406
00:19:44,478 --> 00:19:45,478
Yeah but...

407
00:19:46,581 --> 00:19:50,252
everyone needs a place to come
to be alone.

408
00:19:50,318 --> 00:19:52,596
Look at me! Yeah! Look at me!

409
00:19:57,086 --> 00:19:59,011
Get a clown over here, please!

410
00:20:00,754 --> 00:20:01,806
You're a loner?

411
00:20:01,953 --> 00:20:03,644
I'm a loner all right.

412
00:20:05,346 --> 00:20:06,555
I'm just a...

413
00:20:07,515 --> 00:20:09,154
lonely loner

414
00:20:09,966 --> 00:20:12,044
on a lonely road.

415
00:20:14,320 --> 00:20:15,576
Alone.

416
00:20:17,235 --> 00:20:18,779
Do you always wear shades?

417
00:20:19,237 --> 00:20:20,237
Always.

418
00:20:21,710 --> 00:20:23,336
They help me hide the fact

419
00:20:23,942 --> 00:20:24,942
I'm always lying

420
00:20:26,751 --> 00:20:27,751
to women.

421
00:20:34,959 --> 00:20:39,022
<i>Everyone is having having fun, fun, fun.</i>

422
00:20:39,216 --> 00:20:41,716
<i>'Cause everything is nice
and everyone is friendly.</i>

423
00:20:41,717 --> 00:20:44,070
<i>Lots of friendly faces
having fun, fun, fun.</i>

424
00:20:44,071 --> 00:20:46,678
<i>'Cause everything is nice
and everyone is friendly.</i>

425
00:20:46,679 --> 00:20:49,383
<i>- Smile and the world will...</i>
- Excuse me! Excuse me!

426
00:20:49,384 --> 00:20:52,717
It's just... It's not... Sorry,
it's just not appropriate at the moment.

427
00:21:13,790 --> 00:21:15,763
You're that bloke off the telly!

428
00:21:16,453 --> 00:21:19,458
And you must be the woman he phoned!
Brilliant!

429
00:21:19,733 --> 00:21:21,566
Funniest thing I've seen in ages.

430
00:21:33,470 --> 00:21:34,855
God! I'm a bastard!

431
00:21:35,897 --> 00:21:37,937
Yes. So you keep saying.

432
00:21:38,465 --> 00:21:40,649
Trouble... It's basically what I am.

433
00:21:43,961 --> 00:21:44,961
Thank you.

434
00:21:48,164 --> 00:21:49,219
You know something?

435
00:21:50,619 --> 00:21:52,730
I don't think you're a bastard at all.

436
00:21:53,178 --> 00:21:54,178
I am so.

437
00:21:54,819 --> 00:21:56,533
I am a complete tool.

438
00:21:58,951 --> 00:21:59,926
Oh my God!

439
00:22:00,008 --> 00:22:02,216
No don't, don't! Don't hit him!

440
00:22:07,039 --> 00:22:08,039
I know her.

441
00:22:12,539 --> 00:22:14,167
Hey! What's going on?

442
00:22:14,294 --> 00:22:15,783
Stay out of this, Fonzy!

443
00:22:15,835 --> 00:22:17,197
You're fighting in front of kids!

444
00:22:17,244 --> 00:22:18,160
I don't care!

445
00:22:18,183 --> 00:22:19,205
You're a brute!

446
00:22:19,228 --> 00:22:20,872
This bloke just insulted me!

447
00:22:21,457 --> 00:22:24,404
Hey! Bit more careful
with the clown shoe there, Mister.

448
00:22:29,609 --> 00:22:31,171
Just hold your head back.

449
00:22:33,615 --> 00:22:34,836
Oh God!

450
00:22:38,923 --> 00:22:40,311
He caught me by surprise.

451
00:22:42,505 --> 00:22:44,090
When I see him tomorrow...

452
00:22:45,243 --> 00:22:47,539
- That was his last day.
- Thank God!

453
00:22:47,882 --> 00:22:49,509
Oh my God, look!

454
00:22:49,586 --> 00:22:51,539
If you're coming,
can you just bloody hurry up!

455
00:22:51,540 --> 00:22:53,296
- I'm coming! I...
- God's sake!

456
00:22:53,297 --> 00:22:55,130
I just need to take my shoes off.

457
00:22:56,182 --> 00:22:57,638
They look like such fruits.

458
00:23:02,159 --> 00:23:04,238
- I'll call you a cab.
- No, no, no, it's fine.

459
00:23:04,261 --> 00:23:05,616
- Taxi!
- I'll get my bus.

460
00:23:05,617 --> 00:23:07,561
Don't be stupid, it's cold. Taxi!

461
00:23:08,067 --> 00:23:09,272
- Here.
- Oh God.

462
00:23:09,529 --> 00:23:11,279
Here you can have your 20 back.

463
00:23:11,338 --> 00:23:14,930
I don't really think the bet
proved anything. Taxi!

464
00:23:28,850 --> 00:23:29,996
Thanks, Roy.

465
00:23:30,748 --> 00:23:32,351
- I just want...
- Nice date!

466
00:23:36,061 --> 00:23:39,408
Just a lonely loner
walking a lonely road.

467
00:23:44,326 --> 00:23:45,326
<i>Hi, girls.</i>

468
00:23:45,371 --> 00:23:48,455
<i>My name is Maurice Moss.</i>
<i>Or Moss for short.</i>

469
00:23:48,930 --> 00:23:50,267
<i>Sorry about the sound quality</i>

470
00:23:50,302 --> 00:23:53,144
<i>but I'm having to do this</i>
<i>in the toilet of my mom's house.</i>

471
00:23:53,150 --> 00:23:55,761
<i>She's watching</i>
<i>Diagnosis Murder at full volume.</i>

472
00:23:55,923 --> 00:23:57,392
<i>And it drives me up the wall.</i>

473
00:23:58,408 --> 00:24:02,185
<i>Don't worry, though. I'm sitting on</i>
<i>the toilet but I'm not using it.</i>

474
00:24:03,563 --> 00:24:04,416
<i>What?</i>

475
00:24:04,417 --> 00:24:05,417
<i>- Moss?</i>
<i>- Yes?</i>

476
00:24:05,762 --> 00:24:06,964
<i>What you doing in there?</i>

477
00:24:06,965 --> 00:24:08,885
<i>Number 2s, leave me alone!</i>

478
00:24:09,420 --> 00:24:11,071
<i>Don't forget to flush the toilet.</i>

479
00:24:11,072 --> 00:24:12,771
<i>Don't clog it up like you usually do.</i>

480
00:24:12,772 --> 00:24:16,037
<i>I know! Just stop doing this!</i>
<i>You're always doing this!</i>

481
00:24:16,077 --> 00:24:17,799
<i>You're making it going back in!</i>

482
00:24:20,235 --> 00:24:21,557
<i>Now, my type...</i>

483
00:24:23,239 --> 00:24:26,127
Travis, Any1 and Sixe
for FOROM.COM