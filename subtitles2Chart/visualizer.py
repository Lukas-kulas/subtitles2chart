# This class visualize data
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from os import path


class Visualizer:

    def word_chart(self):
        d = "/home/lukaskulas/PycharmProjects/subtitles2chart/subtitles2Chart/subtitlesData/it crowd/"
        text = open(path.join(d, 'The.IT.Crowd.1x02.ws.pdtv.xvid-gothic.EN.srt')).read()
        wordcloud = WordCloud().generate(text)

        # Display the generated image:
        # the matplotlib way:

        plt.imshow(wordcloud)
        plt.axis("off")

        # take relative word frequencies into account, lower max_font_size
        wordcloud = WordCloud(max_font_size=40, relative_scaling=.5).generate(text)
        plt.figure()
        plt.imshow(wordcloud)
        plt.axis("off")
        plt.show()

    def time_chart(self, season):
        x, x1 = [], []
        y, y1 = [], []

        for index, elem in enumerate(season):

            x.append(index)
            x1.append(index)
            y.append(season[index].episode_length.total_seconds())
            y1.append(season[index].quotes_time().total_seconds())

        plt.title("Czas rozmowy - czas odcinka")
        plt.bar(x, y, label="Czas odcinka", color="green")
        plt.bar(x1, y1, label="Czas napisow", color="red")

        plt.show()
