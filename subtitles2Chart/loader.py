# This class import and clean data
import os
import datetime as dt
import requests
import zipfile
import io as ios

class FileReader:

    def get_file_list(self, catalog):
        if catalog == "":
                catalog = "test"
        list_of_files = os.listdir("subtitlesData/" + catalog)
        return list_of_files

    def load_file(self, catalog, file_name):
        # TODO ADD exception handling
        raw_data = []
        SUBTITLES_LOCATION = os.getcwd() + "/subtitlesData/"
        path = SUBTITLES_LOCATION + catalog + "/" + file_name

        with open(path, 'r', encoding='utf-8', errors='ignore') as f:
            raw_data.append(f.read())

        for elem in raw_data:
            data = elem.split("\n\n")

        return data

    def get_subtitle(self, series_name, series_id, seasons):
        for i in range(seasons):
            season = str(i + 1)
            download_link = 'http://eng.tvsubtitles.net/download-' \
                            + series_id + '-' + season + '-en.html'

            r = requests.get(download_link)
            z = zipfile.ZipFile(ios.BytesIO(r.content))
            directory = series_name + '/Season ' + season
            if not os.path.exists(directory):
                os.makedirs(directory)
            z.extractall(path=directory)

    def parse_data(self, data):
        time_stamp = []
        text = []
        to_remove = ["<i>", "</i>", "?", "!", ".", ",", "'", "-"]

        for elem in data:
            parsed = elem.split("\n")

            if len(parsed) >= 2:
                # [0] - index value is useless
                time_markers = parsed[1].replace(",", ".").split(" --> ")
                time_start = \
                    dt.datetime.strptime(time_markers[0], '%H:%M:%S.%f')
                time_stop = \
                    dt.datetime.strptime(time_markers[1], '%H:%M:%S.%f')
                total_duration = time_stop - time_start

                time_stamp.append(total_duration)

                quote = ' '.join(parsed[2:])

                for word in to_remove:
                    quote = quote.replace(word, "")

                quote = quote.replace("  ", " ")
                text.append(quote.strip())

        control_time = dt.datetime.strptime("00:00:00.000", '%H:%M:%S.%f')
        episode_length = time_stop - control_time

        return episode_length, time_stamp, text

    def get_data(self, catalog, file_name):

        data = self.load_file(catalog, file_name)
        episode_length, time_stamp, parsed_data = self.parse_data(data)

        return catalog, file_name, episode_length, time_stamp, parsed_data




